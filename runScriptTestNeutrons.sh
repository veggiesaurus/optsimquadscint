#!/bin/bash
#trap 'kill $(jobs -p)' EXIT
# Batch parameters
N_PROC=4
COMMAND_NAME=./opticalSim
OUTPUT_ALL_THREADS=false
CLEAN_THREAD_OUTPUT=true
JOB_PREFIX=monoenergetic_neutrons_test

# Simulation parameters
SEED=1000
#N_EVENTS=10000000
N_EVENTS=1000000
# Histogram parameters
PSD_TIME=0.25

# Detector parameters
#for optimised run, SCINT_RES=SCINT_RES_ACTUAL*0.547722558
#SCINT_RES=4.4
SCINT_RES=3.286335348
USE_PVT_VALS=(0)
#SOURCE_ENERGY_VALS=(0.25 0.50 0.75 1.00 1.25 1.50 1.75 2.00 2.25 2.50 2.75 3.00 3.25 3.50 3.75 4.00 4.25 4.50 4.75 5.00 5.25 5.50 5.75 6.00 6.25 6.50 6.75 7.00 7.25 7.50 7.75 8.00 8.25 8.50 8.75 9.00 9.25 9.50 9.75 10.00 10.25 10.50 10.75 11.00 11.25 11.50 11.75 12.00)
SOURCE_ENERGY_VALS=(8.00)
# Source parameters
SOURCE_MACRO=source_macros/neutronSourceTest.mac
# additional
#ADDITIONAL_FLAGS="--scintYield 8600 --pmtEff 0.30"
ADDITIONAL_FLAGS="--scintYield 2580 --pmtEff 1.00 --singleScint"


for SOURCE_ENERGY in ${SOURCE_ENERGY_VALS[*]}
do
	for USE_PVT in ${USE_PVT_VALS[*]}
	do
		ADDITIONAL_FLAGS_CURRENT=$ADDITIONAL_FLAGS
		SCINT_TYPE=PST			
		if ((USE_PVT == 1))
		then
			SCINT_TYPE=PVT
			ADDITIONAL_FLAGS_CURRENT+=" --usePVT"
			echo "Using PVT base"
		fi
		#ADDITIONAL_FLAGS_CURRENT+=" --particleEnergyMeV "$SOURCE_ENERGY
		echo $ADDITIONAL_FLAGS_CURRENT
		OUTPUT_SUFFIX_EVENTS=$((N_EVENTS/1000))k
		if (($N_EVENTS >= 1000000))
		then
			OUTPUT_SUFFIX_EVENTS=$((N_EVENTS/1000000))mil
		fi
		OUTPUTFILE=${SCINT_TYPE}_singleScint_neutron_cell_aligned_iso_${OUTPUT_SUFFIX_EVENTS}_Res${SCINT_RES}_${SOURCE_ENERGY}MeV
		echo "Running sim with "$N_EVENTS" events spread across "$N_PROC" processes."
		echo "Outputting results to "$OUTPUTFILE".root"
		N=$[N_EVENTS / N_PROC]
		SEED_END=$[SEED+N_PROC-1]

		echo "Cleaning up previous files"
		eval rm -f QuadScint_{$SEED..$SEED_END}.root    

		for ((i=0;i<N_PROC;i++))
		do
		    # seed
		    SEED_JOB=$[SEED+i]

		    # pipe output
		    PIPE_COMMAND='> /dev/null 2>&1'
		    if [ $i -eq 0 ] || $OUTPUT_ALL_THREADS
		    then
		      PIPE_COMMAND='| sed "s/^/[energy="${SOURCE_ENERGY}MeV"; PVT="$USE_PVT"; thread "$i"] /"'
		    fi
		    eval $COMMAND_NAME --seed $SEED_JOB --numberOfEvents $N --scintRes $SCINT_RES $ADDITIONAL_FLAGS_CURRENT --sourceMacro $SOURCE_MACRO $PIPE_COMMAND &

		done

		wait
		eval mkdir -p ${JOB_PREFIX}
		eval hadd -f6 ${JOB_PREFIX}/${OUTPUTFILE}.root QuadScint_{$SEED..$SEED_END}.root


		if $CLEAN_THREAD_OUTPUT
		then
		   eval rm -f QuadScint_{$SEED..$SEED_END}.root    
		fi
	done
done
