#!/bin/bash
#trap 'kill $(jobs -p)' EXIT
# Batch parameters

N_PROC=4
Z_ROT=0
N_EV_MIL=80
JOB_NAME=dummyTest
FILL_WIDTH=44
FILL_THICKNESS=11.5
SEED=1000
HADD_OUTPUT=true
SOURCE_MACRO=source_macros/ambe_neutrons_above.mac
SCINT_RES=4.0
OUTPUT_PREFIX=PST_ambe
ENERGY_OVERRIDE=-1
ENERGY_OVERRIDE_SPREAD=0

while getopts ":z:n:t:w:r:s:N:J:H:S:O:E:e:" opt; do
  case $opt in    
    z)
      Z_ROT=$OPTARG
      ;;    
    n)
      N_EV_MIL=$OPTARG
      ;;
    t)
      FILL_THICKNESS=$OPTARG
      ;;    
    w)
      FILL_WIDTH=$OPTARG
      ;;
    r)
      SCINT_RES=$OPTARG
      ;;
    s)
      SEED=$OPTARG
      ;;
    N)
      N_PROC=$OPTARG      
      ;;
    J)
      JOB_NAME=$OPTARG
      ;;
    H)
      HADD_OUTPUT=$OPTARG
      ;;
   S)
      SOURCE_MACRO=$OPTARG
      ;;   
   O)
      OUTPUT_PREFIX=$OPTARG
      ;;
   E)
      ENERGY_OVERRIDE=$OPTARG
      ;;
    e)
      ENERGY_OVERRIDE_SPREAD=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

COMMAND_NAME=./opticalSim
OUTPUT_ALL_THREADS=false
CLEAN_THREAD_OUTPUT=true

# Simulation parameters
N_EVENTS=$(($N_EV_MIL*1000000))
# Histogram parameters
PSD_TIME=0.25

# Detector parameters
OPT_SIM=true

#for optimised run, SCINT_RES=SCINT_RES_ACTUAL*0.547722558
OPT_RES=$SCINT_RES
ADDITIONAL_FLAGS="--scintYield 8600 --pmtEff 0.30"
if $OPT_SIM
then
   OPT_RES=$(bc <<< 'scale=2; '$SCINT_RES'*0.547722558')
   ADDITIONAL_FLAGS="--scintYield 2580 --pmtEff 1.00"
fi


OUTPUTFILE=${OUTPUT_PREFIX}_${Z_ROT}_deg_${N_EV_MIL}mil_Res${SCINT_RES}

echo "Running sim with "$N_EVENTS" events spread across "$N_PROC" processes."
echo "Input source macro: "$SOURCE_MACRO
echo "Detector Z-rotation @"$Z_ROT" degrees."
echo "Output will be placed in "${JOB_NAME}/${OUTPUTFILE}".root"
if $OPT_SIM
then
   echo "Scintillator resolution is "$SCINT_RES" (optimised res of "$OPT_RES")"
else
   echo "Scintillator resolution is "$SCINT_RES
fi


N=$[N_EVENTS / N_PROC]
SEED_END=$[SEED+N_PROC-1]

echo "Cleaning up previous files"
eval rm -f QuadScint_{$SEED..$SEED_END}.root    

for ((i=0;i<N_PROC;i++))
do
    # seed
    SEED_JOB=$[SEED+i]

    # pipe output
    PIPE_COMMAND='> /dev/null 2>&1'
    if [ $i -eq 0 ] || $OUTPUT_ALL_THREADS
    then
      PIPE_COMMAND='| sed "s/^/[thread "$i" of "$N_PROC" (zRot: "$Z_ROT"; job: "$JOB_NAME")] /"'
    fi    
    eval $COMMAND_NAME --seed $SEED_JOB --numberOfEvents $N --zRot $Z_ROT --particleEnergyMeV $ENERGY_OVERRIDE --particleEnergySpreadMeV $ENERGY_OVERRIDE_SPREAD  --scintRes $OPT_RES --fillWidth $FILL_WIDTH --fillThickness $FILL_THICKNESS $ADDITIONAL_FLAGS --sourceMacro $SOURCE_MACRO $PIPE_COMMAND &
done

wait
mkdir -p ${JOB_NAME}

if $HADD_OUTPUT
then
    eval hadd -f6 -v 0 ${JOB_NAME}/${OUTPUTFILE}.root QuadScint_{$SEED..$SEED_END}.root
else
    for ((i=0;i<N_PROC;i++))
    do
        # seed
        SEED_JOB=$[SEED+i]        
        cp QuadScint_${SEED_JOB}.root ${JOB_NAME}/${OUTPUTFILE}_${SEED_JOB}.root
    done
fi

if $CLEAN_THREAD_OUTPUT
then
   eval rm QuadScint_{$SEED..$SEED_END}.root    
fi
