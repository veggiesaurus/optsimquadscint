#pragma once

#include "G4VSensitiveDetector.hh"

#include "OpticalSimPMTHit.hh"

class G4Step;
class G4HCofThisEvent;

class OpticalSimPMTSensitiveDetector : public G4VSensitiveDetector
{

  public:

    OpticalSimPMTSensitiveDetector(G4String name);
    virtual ~OpticalSimPMTSensitiveDetector();

    virtual void Initialize(G4HCofThisEvent* );
    virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* );

    //A version of processHits that keeps aStep constant
    G4bool ProcessHits_constStep(const G4Step* aStep, G4TouchableHistory* );
    virtual void EndOfEvent(G4HCofThisEvent* );
    virtual void clear();
    void DrawAll();
    void PrintAll();

  private:

    OpticalSimPMTHitsCollection* PMTHitCollection;
};
