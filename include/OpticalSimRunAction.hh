#pragma once

#include "G4UserRunAction.hh"
#include "Analysis.hh"
#include "globals.hh"

class G4Timer;
class G4Run;

struct DetectorGeometryParameters
{
	//scintillator geometry
	G4double scintWidth = 6*mm;
	G4double scintLength = 50*mm;
	G4double wrapThickness = 0.635*mm;
	
	//HDPE filler geometry
	G4double fillThickness = 11.5*mm;
	G4double fillWidth = 44*mm;
	G4double fillLength = 52*mm;
	
	//SiPM and PCB geometry
	G4double sipmThickness = 0.65*mm;
	G4double sipmSeparation = 28*mm;
	G4double boardThickness = 1.5*mm;
	G4double boardWidth = 22*mm;
	
	//housing geometry
	G4double housingLength = 7.5*mm;
	G4double housingWidth = 66*mm;
	G4double housingWallThickness = 1.2*mm;
	G4double housingSpacerThickness = 3.0*mm; 
	G4double housingBaseThickness = 2.0* mm;
	G4double housingPLADensityFactor = 0.80;
	
	//rotation
	G4double zRotationDeg = 0.0;
	
};

struct ScintillatorParameters
{
	G4double photonYield;
	G4double resolution;
	G4double coatingReflectivity;
    G4bool usePVT;
    G4bool singleScint;
};

struct PhotonDetectorParameters
{
	G4double reflectivity;
	G4double efficiency;
};

class OpticalSimRunAction : public G4UserRunAction
{
  public:
	  OpticalSimRunAction(G4int seed, G4int s_numberOfEvents, DetectorGeometryParameters s_geomParameters, ScintillatorParameters s_scintParameters, PhotonDetectorParameters s_photonParameters);
   ~OpticalSimRunAction();

  public:
       virtual void BeginOfRunAction(const G4Run*);
       virtual void EndOfRunAction(const G4Run*);

  private:
	  void fillRunInfoTuples();
    G4Timer* timer;
	G4int seed;
	G4int numberOfEvents;
	DetectorGeometryParameters geomParameters;
	ScintillatorParameters scintParameters;
	PhotonDetectorParameters photonParameters;

};
