#pragma once

#include "G4VUserPrimaryGeneratorAction.hh"

#include "globals.hh"

class G4ParticleGun;
class G4Event;
class G4String;
class G4GeneralParticleSource;

class OpticalSimPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
    OpticalSimPrimaryGeneratorAction();
    ~OpticalSimPrimaryGeneratorAction();

public:
    void GeneratePrimaries(G4Event*);

private:
    G4GeneralParticleSource* source;
    //parameters

};
