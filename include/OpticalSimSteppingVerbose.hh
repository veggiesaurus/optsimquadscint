#pragma once

#include "G4SteppingVerbose.hh"

class OpticalSimSteppingVerbose : public G4SteppingVerbose
{
 public:   

   OpticalSimSteppingVerbose();
  ~OpticalSimSteppingVerbose();

   void StepInfo();
   void TrackingStarted();

};
