#pragma once

#include "G4UserStackingAction.hh"

#include "globals.hh"

class OpticalSimStackingAction : public G4UserStackingAction
{
  public:
    OpticalSimStackingAction();
   ~OpticalSimStackingAction();

  public:
    G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* aTrack);
    void NewStage();
    void PrepareNewEvent();

  private:
    G4int gammaCounter;
};

