#pragma once

#include "G4UserEventAction.hh"

#include "globals.hh"

#include <vector>
#include <ostream>
#include <string>

class G4Timer;

class OpticalSimEventAction: public G4UserEventAction
{
public:
    OpticalSimEventAction(G4int s_numberOfEvents, G4bool s_verboseEvent, G4double s_shortGatePSD);
	virtual ~OpticalSimEventAction();

    virtual void BeginOfEventAction(const G4Event*);
    virtual void EndOfEventAction(const G4Event*);
private:
	G4int PMTCollID, scintCollID;
    G4int numberOfEvents;
    G4bool verboseEvent;
    G4bool isFirstEvent;
    G4int eventCount;
    G4Timer* timer;
    //psd
    G4double shortGatePSD;
    
};
