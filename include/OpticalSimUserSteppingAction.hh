#pragma once

#include "G4UserSteppingAction.hh"

#include "globals.hh"

class OpticalSimUserSteppingAction : public G4UserSteppingAction
{
public:
    OpticalSimUserSteppingAction();
    virtual ~OpticalSimUserSteppingAction();
    virtual void UserSteppingAction(const G4Step* theStep);
};
