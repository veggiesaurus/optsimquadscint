#pragma once

#include "G4VUserDetectorConstruction.hh"
#include "OpticalSimRunAction.hh"
#include "globals.hh"

class G4PVPlacement;
class G4LogicalVolume;

class OpticalSimDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    OpticalSimDetectorConstruction(DetectorGeometryParameters s_geomParameters, ScintillatorParameters s_scintParameters, PhotonDetectorParameters s_photonParameters);
   ~OpticalSimDetectorConstruction();

  public:
    G4VPhysicalVolume* Construct();
    void ConstructSDandField();

  private:

	G4double expHallXY;
    G4double expHallZ;

    DetectorGeometryParameters geomParameters;
    ScintillatorParameters scintParameters;
    PhotonDetectorParameters photonParameters;


    G4PVPlacement* photonDetectorPhys[8];
    G4LogicalVolume* photonDetectorLog;


};
