#pragma once


#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4VPhysicalVolume.hh"

class G4VTouchable;


class OpticalSimScintHit : public G4VHit
{
public:

	OpticalSimScintHit();
	virtual ~OpticalSimScintHit();
	OpticalSimScintHit(const OpticalSimScintHit &right);

	const OpticalSimScintHit& operator=(const OpticalSimScintHit &right);
	G4int operator==(const OpticalSimScintHit &right) const;

	inline void *operator new(size_t);
	inline void operator delete(void *aHit);

	virtual void Draw();
	inline void SetDrawFlag(G4bool b){ drawFlag = b; }
	inline G4bool GetDrawFlag(){ return drawFlag; }

    inline G4double GetEnergy(){ return eDep; }
    inline void AddEnergy(G4double energy){ eDep += energy; }

	inline void SetScintPhysVol(G4VPhysicalVolume* s_physVol){ physVol = s_physVol; }
	inline G4VPhysicalVolume* GetScintPhysVol(){ return physVol; }

	inline G4int GetScintCopyNumber() const { return scintCopyNumber; }
	inline void SetScintCopyNumber(G4int value) { scintCopyNumber = value; }

	inline G4int GetTimeOfFirstHit() const { return timeFirstHit; }
	inline void SetTimeOfFirstHit(G4double value) { timeFirstHit = value; }


private:
	G4int scintCopyNumber;
	G4double eDep;
	G4double timeFirstHit;
	G4VPhysicalVolume* physVol;
	G4bool drawFlag;

};

typedef G4THitsCollection<OpticalSimScintHit> OpticalSimScintHitsCollection;

extern G4Allocator<OpticalSimScintHit> OpticalSimScintHitAllocator;

inline void* OpticalSimScintHit::operator new(size_t){
	void *aHit;
	aHit = (void *)OpticalSimScintHitAllocator.MallocSingle();
	return aHit;
}

inline void OpticalSimScintHit::operator delete(void *aHit){
	OpticalSimScintHitAllocator.FreeSingle((OpticalSimScintHit*)aHit);

}
