#pragma once

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4VPhysicalVolume.hh"

#include <vector>

using namespace std;


class G4VTouchable;

class OpticalSimPMTHit : public G4VHit
{
  public:

    OpticalSimPMTHit();
    virtual ~OpticalSimPMTHit();
    OpticalSimPMTHit(const OpticalSimPMTHit &right);

    const OpticalSimPMTHit& operator=(const OpticalSimPMTHit &right);
    G4int operator==(const OpticalSimPMTHit &right) const;

    inline void *operator new(size_t);
    inline void operator delete(void *aHit);

    virtual void Draw();
    void PrintHitTimes(ostream& out);
    void AddHitTimes(vector<G4double>& hitTimes, vector<G4double>& hitSqTimes, G4int numBins, G4double binInterval);
    G4int GetPhotonCountGated(G4double timeGateStart, G4double timeGateStop);
    inline void SetDrawFlag(G4bool b){drawFlag=b;}
    inline G4bool GetDrawFlag(){return drawFlag;}

    inline void AddPhoton(G4double time){photonHitTimes.push_back(time);}
    inline G4int GetPhotonCount(){return photonHitTimes.size();}

    inline void SetPMTPhysVol(G4VPhysicalVolume* s_physVol){physVol=s_physVol;}
    inline G4VPhysicalVolume* GetPMTPhysVol(){return physVol;}

	inline G4int GetPDCopyNumber() const { return pdCopyNumber; }
	inline void SetPDCopyNumber(G4int value) { pdCopyNumber = value; }


private:
    G4int pdCopyNumber;

	vector<G4double> photonHitTimes;
    G4VPhysicalVolume* physVol;
    G4bool drawFlag;

};

typedef G4THitsCollection<OpticalSimPMTHit> OpticalSimPMTHitsCollection;

extern G4Allocator<OpticalSimPMTHit> OpticalSimPMTHitAllocator;

inline void* OpticalSimPMTHit::operator new(size_t){
  void *aHit;
  aHit = (void *) OpticalSimPMTHitAllocator.MallocSingle();
  return aHit;
}

inline void OpticalSimPMTHit::operator delete(void *aHit){
  OpticalSimPMTHitAllocator.FreeSingle((OpticalSimPMTHit*) aHit);

}
