#pragma once

#include "G4VSensitiveDetector.hh"

#include "OpticalSimScintHit.hh"

class G4Step;
class G4HCofThisEvent;

class OpticalSimScintSensitiveDetector : public G4VSensitiveDetector
{

public:

	OpticalSimScintSensitiveDetector(G4String name);
	virtual ~OpticalSimScintSensitiveDetector();

	virtual void Initialize(G4HCofThisEvent*);
	virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* touchable);

	virtual void EndOfEvent(G4HCofThisEvent*);
	virtual void clear();
	void DrawAll();
	void PrintAll();

private:
	G4int hitCID;
	OpticalSimScintHitsCollection* scintHitCollection;
};
