#!/bin/bash
#trap 'kill $(jobs -p)' EXIT
# Batch parameters
N_PROC=4
COMMAND_NAME=./opticalSim
OUTPUT_ALL_THREADS=false
CLEAN_THREAD_OUTPUT=true
JOB_PREFIX=scint_res_check

# Simulation parameters
SEED=1000
#N_EVENTS=10000000
N_EVENTS=2000000
# Histogram parameters
PSD_TIME=0.25

# Detector parameters
#SCINT_RES_VALS=(4.0 4.2 4.4 4.6 4.8 5.0)
SCINT_RES_VALS=(3.0 3.2 3.4 3.6 3.8 4.0)
USE_PVT_VALS=(0)
# Source parameters
#SOURCE_MACRO=source_na22_cell_aligned_front_iso_disc.mac
SOURCE_MACRO=source_macros/source_cs137_aligned_front_iso.mac
# additional
ADDITIONAL_FLAGS="--scintYield 8600 --pmtEff 0.30"



for SCINT_RES in ${SCINT_RES_VALS[*]}
do
	for USE_PVT in ${USE_PVT_VALS[*]}
	do
		ADDITIONAL_FLAGS_CURRENT=$ADDITIONAL_FLAGS
		SCINT_TYPE=PST				
		if ((USE_PVT == 1))
		then
			SCINT_TYPE=PVT
			ADDITIONAL_FLAGS_CURRENT+=" --usePVT"
			echo "Using PVT base"
		fi		
		OUTPUT_SUFFIX_EVENTS=$((N_EVENTS/1000))k
		if (($N_EVENTS >= 1000000))
		then
			OUTPUT_SUFFIX_EVENTS=$((N_EVENTS/1000000))mil
		fi
		OUTPUTFILE=${SCINT_TYPE}_cs137_cell_aligned_iso_${OUTPUT_SUFFIX_EVENTS}_Res${SCINT_RES}
		echo "Running sim with "$N_EVENTS" events spread across "$N_PROC" processes."
		echo "Outputting results to "$OUTPUTFILE".root"
		N=$[N_EVENTS / N_PROC]
		SEED_END=$[SEED+N_PROC-1]

		echo "Cleaning up previous files"
		eval rm -f QuadScint_{$SEED..$SEED_END}.root    

		for ((i=0;i<N_PROC;i++))
		do
		    # seed
		    SEED_JOB=$[SEED+i]

		    # pipe output
		    PIPE_COMMAND='> /dev/null 2>&1'
		    if [ $i -eq 0 ] || $OUTPUT_ALL_THREADS
		    then
		      PIPE_COMMAND='| sed "s/^/[res="$SCINT_RES"; PVT="$USE_PVT"; thread "$i"] /"'
		    fi		    
		    eval $COMMAND_NAME --seed $SEED_JOB --numberOfEvents $N --scintRes $SCINT_RES $ADDITIONAL_FLAGS_CURRENT --sourceMacro $SOURCE_MACRO $PIPE_COMMAND &

		done

		wait
		eval mkdir -p ${JOB_PREFIX}
		eval hadd -f6 ${JOB_PREFIX}/${OUTPUTFILE}.root QuadScint_{$SEED..$SEED_END}.root


		if $CLEAN_THREAD_OUTPUT
		then
		   eval rm -f QuadScint_{$SEED..$SEED_END}.root    
		fi
	done
done
