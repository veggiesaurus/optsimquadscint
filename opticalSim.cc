#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "QGSP_BIC_HP.hh"
#include "G4OpticalPhysics.hh"

#include "Analysis.hh"

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include "OpticalSimDetectorConstruction.hh"
#include "OpticalSimPrimaryGeneratorAction.hh"
#include "OpticalSimRunAction.hh"
#include "OpticalSimEventAction.hh"
#include "OpticalSimStackingAction.hh"
#include "OpticalSimUserSteppingAction.hh"
#include "OpticalSimSteppingVerbose.hh"

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <iterator>
#include <string>

namespace po = boost::program_options;

using namespace std;

po::variables_map vm;
//detector parameters
DetectorGeometryParameters geomParameters;
ScintillatorParameters scintParameters;
PhotonDetectorParameters photonParameters;

//gun parameters
G4double particleEnergyMeV, particleEnergySpreadMeV;
//output and run parameters
G4int numberOfEvents,seed;
G4bool verboseGen, verboseTracking, verboseEvents, printHitTimes , vis;
//histograms
G4int photonLowerCut, photonUpperCut;

G4double shortGatePSD;

//filenames
std::string outputFileHitCounts, outputFileHitBins, outputFilePSDHist;
//custom macros
std::string sourceMacro;

bool parseVariables(int argc, char* argv[])
{
    try {
        po::options_description desc("Allowed options");
        desc.add_options()
                ("help", "produce help message")
                ("numberOfEvents,N", po::value<G4int>(&numberOfEvents)->default_value(0), "set number of events")

                ("width,W", po::value<G4double>(&geomParameters.scintWidth)->default_value(6.0), "set width and height of scintillator (in mm)")
                ("length,L", po::value<G4double>(&geomParameters.scintLength)->default_value(50.0), "set length of scintillator (in mm)")                
                ("fillThickness,F", po::value<G4double>(&geomParameters.fillThickness)->default_value(11.5), "set thickness of perspex filler (in mm)")
                ("fillWidth", po::value<G4double>(&geomParameters.fillWidth)->default_value(44.0), "set thickness of perspex filler (in mm)")
                ("zRot", po::value<G4double>(&geomParameters.zRotationDeg)->default_value(0.0), "set z rotation of detector (in deg)")

                ("scintYield", po::value<G4double>(&scintParameters.photonYield)->default_value(8600), "set scintillator yield (in photons per MeV)")
                ("scintRes", po::value<G4double>(&scintParameters.resolution)->default_value(5.6), "set scintillator resolution factor")
                ("scintRefl", po::value<G4double>(&scintParameters.coatingReflectivity)->default_value(0.98), "set reflectivity of scintillator wrapping (Al: 0.90, ESR: 0.98)")
                ("pmtEff", po::value<G4double>(&photonParameters.efficiency)->default_value(0.30), "set efficiency of photon detector")
                ("pmtRefl", po::value<G4double>(&photonParameters.reflectivity)->default_value(0.0), "set reflectivity of photon detector")
                
                

                //("numBinsHitTimes", po::value<G4int>(&numBinsHitTimes)->default_value(-1), "set number of bins to use for average signal histogram (<0: no histogram)")
                //("binIntervalHitTimes", po::value<G4double>(&binIntervalHitTimes)->default_value(0.25), "set bin width (in ns) for average signal histogram")
                ("photonLowerCut,l", po::value<G4int>(&photonLowerCut)->default_value(-1), "set lower bound on number of photons for average signal histogram (<0: no cut)")
                ("photonUpperCut,u", po::value<G4int>(&photonUpperCut)->default_value(-1), "set upper bound on number of photons for average signal histogram (<0: no cut)")

                ("shortGatePSD", po::value<G4double>(&shortGatePSD)->default_value(25), "set short gate (in ns) for PSD histogram")
                ("particleEnergyMeV", po::value<G4double>(&particleEnergyMeV)->default_value(-1), "override source particle energy")
                ("particleEnergySpreadMeV", po::value<G4double>(&particleEnergySpreadMeV)->default_value(0), "override source particle energy spread")

                ("usePVT", "use polyvinyl touline as scintillator base instead of polystyrene")
                ("singleScint", "Use only a single scintillator (RT)")
                ("verboseTracking", "verbose output from particle tracking")
                ("verboseEvents", "verbose output from event action")
                ("vis,V", "visualise events")
                ("printHitTimes", "print out hit times from first event")                

                ("outputFileHitCounts", po::value<std::string>(&outputFileHitCounts)->default_value(""), "set output file for summary of hit photon counts (empty: screen)")
                ("outputFileHitBins", po::value<std::string>(&outputFileHitBins)->default_value(""), "set output file for output of average signal histogram (empty: screen)")
                ("outputFilePSDHist", po::value<std::string>(&outputFilePSDHist)->default_value(""), "set output file for PSD histogram (L vs S) (empty: screen)")

                //custom macros
                ("sourceMacro,s", po::value<std::string>(&sourceMacro)->default_value(""), "set source macro file (empty: default source.mac)")
                ("seed,S", po::value<G4int>(&seed)->default_value(345354), "set random number seed")              
                ;

        po::positional_options_description p;                
        po::store(po::command_line_parser(argc, argv).
         options(desc).positional(p).run(), vm);
        po::notify(vm);


        if (vm.count("help")) {
            G4cout << desc << "\n";
            return 1;
        }        
        verboseGen=vm.count("verboseGen");
        verboseTracking=vm.count("verboseTracking");
        verboseEvents=vm.count("verboseEvents");
        printHitTimes =vm.count("printHitTimes");
        scintParameters.usePVT =vm.count("usePVT");
        scintParameters.singleScint=vm.count("singleScint");
        vis=vm.count("vis");        

    }
    catch(exception& e)
    {
        cerr << "error: " << e.what() << "\n";
        return 1;
    }
    catch(...)
    {
        cerr << "Exception of unknown type!\n";
    }
    return 0;
}


int main(int argc,char** argv)
{
    if (parseVariables(argc, argv))
        return 1;

    CLHEP::RanluxEngine defaultEngine( 1234567, 4 );
    G4Random::setTheEngine( &defaultEngine );
    G4Random::setTheSeed( seed );
    G4cout<<"Seeding with "<<seed<<G4endl;
    // User Verbose output class
    //
    G4VSteppingVerbose* verbosity = new OpticalSimSteppingVerbose;
    G4VSteppingVerbose::SetInstance(verbosity);

    // Run manager
    //
    G4RunManager* runManager = new G4RunManager;

    // UserInitialization classes - mandatory

    G4VModularPhysicsList* physicsList = new QGSP_BIC_HP;
    G4OpticalPhysics* opticalPhysics = new G4OpticalPhysics();
    opticalPhysics->SetYieldRatioByParticleType(true);
    opticalPhysics->SetScintillationByParticleType(true);
    physicsList->RegisterPhysics(opticalPhysics);
    opticalPhysics->SetTrackSecondariesFirst(kCerenkov,false);
    opticalPhysics->SetTrackSecondariesFirst(kScintillation,false);
    runManager->SetUserInitialization(physicsList);



    //
    G4VUserPrimaryGeneratorAction* gen_action = new OpticalSimPrimaryGeneratorAction();
    runManager->SetUserAction(gen_action);
    //
    G4VUserDetectorConstruction* detector = new OpticalSimDetectorConstruction(geomParameters, scintParameters, photonParameters);
    runManager-> SetUserInitialization(detector);

#ifdef G4VIS_USE
    // visualization manager
    //
    G4VisManager* visManager = new G4VisExecutive;
    visManager->Initialize();
#endif

    // UserAction classes
    //
    G4UserRunAction* run_action = new OpticalSimRunAction(seed, numberOfEvents, geomParameters, scintParameters, photonParameters);
    runManager->SetUserAction(run_action);
    //
    G4UserStackingAction* stacking_action = new OpticalSimStackingAction;
    runManager->SetUserAction(stacking_action);

    G4UserSteppingAction* stepping_action = new OpticalSimUserSteppingAction;
    runManager->SetUserAction(stepping_action);

    OpticalSimEventAction* event_action = new OpticalSimEventAction(numberOfEvents, verboseEvents, shortGatePSD);
    runManager->SetUserAction(event_action);

    // Initialize G4 kernel
    //
    runManager->Initialize();
    
	//fill run info tuples

    // Get the pointer to the User Interface manager
    //
    G4UImanager* UImanager = G4UImanager::GetUIpointer();


    //source config
    if (sourceMacro.size())
        UImanager->ApplyCommand(std::string("/control/execute ")+sourceMacro);
    else
        UImanager->ApplyCommand("/control/execute source_macros/dummySourceAbove.mac");
    if (verboseTracking)
        UImanager->ApplyCommand("/tracking/verbose 1");
    else
        UImanager->ApplyCommand("/tracking/verbose 0");

    if (particleEnergyMeV>=0)
    {
        G4cout<<"Uniform energy between "<<(particleEnergyMeV-particleEnergySpreadMeV/2.0)<<" MeV and "<<particleEnergyMeV+particleEnergySpreadMeV/2.0<<" MeV (uniform)"<<G4endl;
        UImanager->ApplyCommand(std::string("/gps/ene/min ") + std::to_string(particleEnergyMeV-particleEnergySpreadMeV/2.0) + std::string(" MeV"));
        UImanager->ApplyCommand(std::string("/gps/ene/max ") + std::to_string(particleEnergyMeV+particleEnergySpreadMeV/2.0) + std::string(" MeV"));
    }

    if (!numberOfEvents)   // Define UI session for interactive mode if numberOfEvents is not specified
    {
#ifdef G4UI_USE
        G4UIExecutive * ui = new G4UIExecutive(argc,argv);
#ifdef G4VIS_USE
        //Bug with QT + OpenGL and G4.10!
        //UImanager->ApplyCommand("/control/execute vis.mac");
#endif
        if (vis)
            UImanager->ApplyCommand("/vis/enable 1");
        else
            UImanager->ApplyCommand("/vis/enable 0");

        ui->SessionStart();

        delete ui;
#endif
    }
    else         // Batch mode
    {
        G4String command = "/run/beamOn ";
        UImanager->ApplyCommand(command+G4UIcommand::ConvertToString(numberOfEvents));
    }  

    // Job termination
    // Free the store: user actions, physics_list and detector_description are
    //                 owned and deleted by the run manager, so they should not
    //                 be deleted in the main() program !

#ifdef G4VIS_USE
    delete visManager;
#endif
    delete runManager;
    delete verbosity;

    return 0;
}
