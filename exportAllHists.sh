#!/bin/bash
trap 'kill $(jobs -p)' EXIT

JOB_PREFIX=$1
NUM_BINS=${2:-100}
UPPER_BOUNDS=${3:-8000}
NUM_DATASETS=${4:-1}
echo $NUM_BINS
mkdir -p $JOB_PREFIX/exports
cd $JOB_PREFIX
echo $NUM_DATASETS
for f in *.root;
do
	echo "Processing $f"
	
	if [ $NUM_DATASETS -eq 1 ]
	then
		root -l -b -q '../exportHist.C ("'$f'", "exports/'$f'_LT.dat", "LT", '$NUM_BINS', '$UPPER_BOUNDS')' &
		root -l -b -q '../exportHist.C ("'$f'", "exports/'$f'_RT.dat", "RT", '$NUM_BINS', '$UPPER_BOUNDS')' &
		root -l -b -q '../exportHist.C ("'$f'", "exports/'$f'_LB.dat", "LB", '$NUM_BINS', '$UPPER_BOUNDS')' &
		root -l -b -q '../exportHist.C ("'$f'", "exports/'$f'_RB.dat", "RB", '$NUM_BINS', '$UPPER_BOUNDS')'
		wait
	else		
		for ((i=0;i<NUM_DATASETS;i+=1))
		do
			root -l -b -q '../exportHist.C ("'$f'", "exports/'$f'_LT_'$i'.dat", "LT", '$NUM_BINS', '$UPPER_BOUNDS', '$NUM_DATASETS', '$i')'> /dev/null & 
			root -l -b -q '../exportHist.C ("'$f'", "exports/'$f'_RT_'$i'.dat", "RT", '$NUM_BINS', '$UPPER_BOUNDS', '$NUM_DATASETS', '$i')'> /dev/null &
			root -l -b -q '../exportHist.C ("'$f'", "exports/'$f'_LB_'$i'.dat", "LB", '$NUM_BINS', '$UPPER_BOUNDS', '$NUM_DATASETS', '$i')'> /dev/null &
			root -l -b -q '../exportHist.C ("'$f'", "exports/'$f'_RB_'$i'.dat", "RB", '$NUM_BINS', '$UPPER_BOUNDS', '$NUM_DATASETS', '$i')'> /dev/null
			wait
		done
	fi
done

wait

