#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4ios.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"
#include "OpticalSimScintSensitiveDetector.hh"
#include "OpticalSimScintHit.hh"
#include "G4SystemOfUnits.hh"
#include "G4SDManager.hh"
OpticalSimScintSensitiveDetector::OpticalSimScintSensitiveDetector(G4String name)
	: G4VSensitiveDetector(name), scintHitCollection(0)
{
	hitCID = -1;
	collectionName.insert("scintHitCollection");
}

OpticalSimScintSensitiveDetector::~OpticalSimScintSensitiveDetector()
{

}

void OpticalSimScintSensitiveDetector::Initialize(G4HCofThisEvent* hitsCE)
{
	scintHitCollection = new OpticalSimScintHitsCollection(SensitiveDetectorName, collectionName[0]);
	//Store collection with event and keep ID
	if (hitCID < 0)
	{
		hitCID = G4SDManager::GetSDMpointer()->GetCollectionID(scintHitCollection);
	}
	hitsCE->AddHitsCollection(hitCID, scintHitCollection);	
}

G4bool OpticalSimScintSensitiveDetector::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
    G4StepPoint* preStep = aStep->GetPreStepPoint();
    G4TouchableHistory* touchable = (G4TouchableHistory*)(preStep->GetTouchable());
	
	//need to know if this is a proton, electron, positron
 	auto definition = aStep->GetTrack()->GetDefinition();
 	if (definition != G4Proton::ProtonDefinition() && definition != G4Positron::PositronDefinition() && definition != G4Electron::ElectronDefinition())
 		return false;
	//G4double edep = -aStep->GetDeltaEnergy();
	G4double edep = aStep->GetTotalEnergyDeposit();
	if (fabs(edep) <= 0.1*eV)
	    return false;
	G4VPhysicalVolume* physVol = aStep->GetPreStepPoint()->GetPhysicalVolume();    
	G4int scintCopyNo = touchable->GetCopyNumber(2);
    G4String name=physVol->GetName();

	//Find the correct hit collection
	G4int n = scintHitCollection->entries();
	OpticalSimScintHit* hit = NULL;
	for (G4int i = 0; i < n; i++)
	{
		if ((*scintHitCollection)[i]->GetScintCopyNumber() == scintCopyNo)
		{			
			hit = (*scintHitCollection)[i];			
			break;
		}
	}

	//this pd wasnt previously hit in this event
	if (hit == NULL)
	{
		hit = new OpticalSimScintHit(); //so create new hit
		hit->SetScintPhysVol(physVol);
		hit->SetScintCopyNumber(scintCopyNo);
		hit->SetTimeOfFirstHit(preStep->GetGlobalTime());
		scintHitCollection->insert(hit);
	}

	hit->AddEnergy(edep);
    //hit->SetDrawFlag(true);
    return true;
}

void OpticalSimScintSensitiveDetector::EndOfEvent(G4HCofThisEvent*)
{

}

void OpticalSimScintSensitiveDetector::clear()
{

}

void OpticalSimScintSensitiveDetector::DrawAll()
{

}

void OpticalSimScintSensitiveDetector::PrintAll()
{

}
