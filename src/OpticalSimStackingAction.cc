#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4Track.hh"
#include "G4ios.hh"

#include "OpticalSimStackingAction.hh"

OpticalSimStackingAction::OpticalSimStackingAction()
: gammaCounter(0)
{

}

OpticalSimStackingAction::~OpticalSimStackingAction()
{

}

G4ClassificationOfNewTrack OpticalSimStackingAction::ClassifyNewTrack(const G4Track * aTrack)
{
  if(aTrack->GetDefinition() == G4OpticalPhoton::OpticalPhotonDefinition())
  { // particle is optical photon
    if(aTrack->GetParentID()>0)
    { // particle is secondary
      gammaCounter++;
    }
  }
  return fUrgent;
}

void OpticalSimStackingAction::NewStage()
{
  //G4cout << "Number of optical photons produced in this event : "<< gammaCounter << G4endl;
}

void OpticalSimStackingAction::PrepareNewEvent()
{
    gammaCounter = 0;
}
