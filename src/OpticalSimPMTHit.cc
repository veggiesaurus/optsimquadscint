#include "G4ios.hh"
#include "G4VVisManager.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"

#include "OpticalSimPMTHit.hh"

G4Allocator<OpticalSimPMTHit> OpticalSimPMTHitAllocator;

OpticalSimPMTHit::OpticalSimPMTHit()
	: physVol(0), drawFlag(false), pdCopyNumber(-1)
{
	photonHitTimes.empty();
}

OpticalSimPMTHit::~OpticalSimPMTHit()
{

}

OpticalSimPMTHit::OpticalSimPMTHit(const OpticalSimPMTHit &right) : G4VHit()
{
    photonHitTimes=right.photonHitTimes;
    physVol=right.physVol;
	pdCopyNumber = right.pdCopyNumber;
    drawFlag=right.drawFlag;
}

const OpticalSimPMTHit& OpticalSimPMTHit::operator=(const OpticalSimPMTHit &right)
{    
    photonHitTimes=right.photonHitTimes;
    physVol=right.physVol;
	pdCopyNumber = right.pdCopyNumber;
    drawFlag=right.drawFlag;
    return *this;
}

G4int OpticalSimPMTHit::operator==(const OpticalSimPMTHit &right) const
{
    return (pdCopyNumber==right.pdCopyNumber);
}

void OpticalSimPMTHit::Draw()
{
    if(drawFlag && physVol)
    {
        G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
        if(pVVisManager)
        {
            //draw in red
            G4VisAttributes attribs(G4Colour(1.,0.,0.));
            attribs.SetForceSolid(true);
            G4RotationMatrix rot;
            if(physVol->GetRotation())//If a rotation is defined use it
                rot=*(physVol->GetRotation());
            G4Transform3D trans(rot,physVol->GetTranslation());//Create transform
            pVVisManager->Draw(*physVol,attribs,trans);//Draw it
        }
    }
}

void OpticalSimPMTHit::PrintHitTimes(ostream& out)
{
    int hitCount=photonHitTimes.size();
    for (int i=0;i<hitCount;i++)
    {
        out<<i<<" \t"<<photonHitTimes[i]<<G4endl;
    }
}

G4int OpticalSimPMTHit::GetPhotonCountGated(G4double timeGateStart, G4double timeGateStop)
{
    int hitCount=photonHitTimes.size();
    int hitCountGated=0;
    for (int i=0;i<hitCount;i++)
    {
        if (photonHitTimes[i]>= timeGateStart && photonHitTimes[i]<= timeGateStop)
            hitCountGated++;
    }
    return hitCountGated;
}

void OpticalSimPMTHit::AddHitTimes(vector<G4double>& hitTimes, vector<G4double>& hitSqTimes, G4int numBins, G4double binInterval)
{
    int hitCount=photonHitTimes.size();
    if (!hitCount)
        return;
    vector<G4double> tempHits;
    tempHits.resize(hitTimes.size());
    for (int i=0;i<hitCount;i++)
    {
        G4int index=(G4int)(photonHitTimes[i]/binInterval);
        if (index>=0 && index<numBins)
            tempHits[index]++;
    }

    for (int i=0;i<hitTimes.size();i++)
    {
        tempHits[i]/=(hitCount/100.0);
        hitTimes[i]+=tempHits[i];
        hitSqTimes[i]+=tempHits[i]*tempHits[i];
    }
}
