#include "G4Material.hh"
#include "G4Element.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4OpticalSurface.hh"
#include "G4Box.hh"
#include "G4SubtractionSolid.hh"
#include "G4Tubs.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4VisAttributes.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4SDManager.hh"
#include "G4VSensitiveDetector.hh"
#include "G4NistManager.hh"
#include "G4RotationMatrix.hh"

#include "OpticalSimDetectorConstruction.hh"
#include "OpticalSimPMTSensitiveDetector.hh"
#include "OpticalSimScintSensitiveDetector.hh"


OpticalSimDetectorConstruction::OpticalSimDetectorConstruction(DetectorGeometryParameters s_geomParameters, ScintillatorParameters s_scintParameters, PhotonDetectorParameters s_photonParameters)
    :geomParameters(s_geomParameters), scintParameters(s_scintParameters), photonParameters(s_photonParameters)
{

    expHallXY = geomParameters.housingWidth*3.0f;
    //leave space in Z for source
    expHallZ = std::max(expHallXY, geomParameters.scintLength*5.0f);

}

OpticalSimDetectorConstruction::~OpticalSimDetectorConstruction()
{

}

G4VPhysicalVolume* OpticalSimDetectorConstruction::Construct()
{

    //------------------------------------------------------ materials
    G4double a, z;
    G4double density;
    G4int ncomponents, natoms, nisotopes;

    //Elements
    G4Element* H = new G4Element("Hydrogen", "H", z=1., a= 1.008*g/mole);
    G4Element* C = new G4Element("Carbon"  , "C", z=6., a= 12.01*g/mole);
    G4Element* O = new G4Element("Oxygen", "O", z=8., a= 15.9994*g/mole);
    G4Element* N = new G4Element("Nitrogen", "N", z=7., a= 14.0067*g/mole);

    //Polyvinyl tourine matrix
    auto PVT = new G4Material("PVT", density= 1032*mg/cm3, ncomponents=2, kStateSolid);
    PVT->AddElement(H, 8.5*perCent);
    PVT->AddElement(C, 91.5*perCent);

    //Polystyrene matrix
    auto PST = new G4Material("PST", density= 1060*mg/cm3, ncomponents=2, kStateSolid);
    PST->AddElement(H, 7.7418*perCent);
    PST->AddElement(C, 92.2582*perCent);

    //PPO dye
    auto PPO = new G4Material("PPO", density= 1094*mg/cm3, ncomponents=4, kStateSolid);
    PPO->AddElement(C, natoms=15);
    PPO->AddElement(H, natoms=11);
    PPO->AddElement(N, natoms=1);
    PPO->AddElement(O, natoms=1);

    //DPA dye
    auto DPA = new G4Material("DPA", density= 1220*mg/cm3, ncomponents=2, kStateSolid);
    DPA->AddElement(C, natoms=26);
    DPA->AddElement(H, natoms=18);
	
    //Density is an estimate.
    auto PVT_ej299 = new G4Material("EJ299-33", density= 1080*mg/cm3, ncomponents=3, kStateSolid);
    if (scintParameters.usePVT)
        PVT_ej299->AddMaterial(PVT, 69.8*perCent);
    else
        PVT_ej299->AddMaterial(PST, 69.8*perCent);

    PVT_ej299->AddMaterial(PPO, (30)*perCent);
    PVT_ej299->AddMaterial(DPA, 0.2*perCent);

    auto PVT_ne102 = new G4Material("EJ-212", density= 1023*mg/cm3, ncomponents=1, kStateSolid);
    PVT_ne102->AddMaterial(PVT, 100*perCent);

    //todo: find NE213 again

    density     = CLHEP::universe_mean_density;                //from PhysicalConstants.h
    G4double pressure    = 1.e-19*pascal;
    G4double temperature = 0.1*kelvin;
    auto vac = new G4Material("Galactic", z=1., a=1.01*g/mole, density, kStateGas,temperature,pressure);

    G4NistManager* man = G4NistManager::Instance();
    auto Air  = man->FindOrBuildMaterial("G4_AIR");
    auto Silicon  = man->FindOrBuildMaterial("G4_Si");
    auto Glass = man->FindOrBuildMaterial("G4_PLEXIGLASS");
    auto HDPE = man->FindOrBuildMaterial("G4_POLYETHYLENE");
    auto PCB = man->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
    
    auto Al=new G4Material("Al",z=13.,a=26.98*g/mole,density=2.70*g/cm3);
    auto Pb=new G4Material("Pb",z=82.,a=207.2*g/mole,density=11.34*g/cm3);
    
    //todo: add perspex material
    
    //PPO dye
    auto PLA = new G4Material("PLA", density= geomParameters.housingPLADensityFactor * 1300*mg/cm3, ncomponents=3, kStateSolid);
    PLA->AddElement(C, natoms=3);
    PLA->AddElement(H, natoms=4);    
    PLA->AddElement(O, natoms=2);

    //Rotation matrices    
    G4RotationMatrix* rotZ90=new G4RotationMatrix();
    rotZ90->rotateZ(90*deg);
    G4RotationMatrix* rotZ180=new G4RotationMatrix();
    rotZ180->rotateZ(180*deg);
    G4RotationMatrix* rotZ270=new G4RotationMatrix();
    rotZ270->rotateZ(270*deg);
    G4RotationMatrix* rotY180=new G4RotationMatrix();
    rotY180->rotateY(180*deg);
    
    G4RotationMatrix* rotZCustom = new G4RotationMatrix();
    rotZCustom->rotateZ(geomParameters.zRotationDeg*deg);
    
#pragma region Volumes

    // The experimental Hall
	auto expHallBox = new G4Box("World", expHallXY*2, expHallXY*2, expHallZ*2);
    auto expHallLog = new G4LogicalVolume(expHallBox,Air,"World",0,0,0);
    auto expHallVA = new G4VisAttributes(G4Colour(0,0,0));
    expHallLog->SetVisAttributes(expHallVA);
    auto expHallPhys = new G4PVPlacement(0,G4ThreeVector(),expHallLog,"World",0,false,0);

    // The experimental Hall
	auto detectorBox = new G4Box("World2", expHallXY, expHallXY, expHallZ);
    auto detectorLog = new G4LogicalVolume(detectorBox,Air,"DetectorLog",0,0,0);
    auto detectorVA = new G4VisAttributes(G4Colour(0,0,0));
    detectorLog->SetVisAttributes(detectorVA);
    auto detectorPhys = new G4PVPlacement(rotZCustom,G4ThreeVector(),detectorLog,"World2",expHallLog,false,0);


     // Filler
    auto fillerBox = new G4Box("Filler", geomParameters.fillWidth/2.0f, geomParameters.fillThickness/2.0f, geomParameters.fillLength/2.0f);
    auto fillerLog = new G4LogicalVolume(fillerBox,HDPE,"Filler",0,0,0);
    auto fillerVA = new G4VisAttributes(G4Colour(1.0,0.7,0.7));
    fillerLog->SetVisAttributes(fillerVA);
    auto fillerPhys = new G4PVPlacement(0,G4ThreeVector(),fillerLog,"Filler",detectorLog,false,0);

    // FillerAlt
    auto fillerBox2 = new G4Box("Filler2", geomParameters.fillThickness/2.0f, (geomParameters.fillWidth - geomParameters.fillThickness)/4.0f, geomParameters.fillLength/2.0f);
    auto fillerLog2 = new G4LogicalVolume(fillerBox2,HDPE,"Filler2",0,0,0);
    fillerLog2->SetVisAttributes(fillerVA);
    auto fillerPhys2A = new G4PVPlacement(0,G4ThreeVector(0.0f, (geomParameters.fillWidth - geomParameters.fillThickness)/4.0f + geomParameters.fillThickness/2.0f, 0.0f),fillerLog2,"Filler2A",detectorLog,false,0);
    auto fillerPhys2B = new G4PVPlacement(rotZ180,G4ThreeVector(0.0f, -(geomParameters.fillWidth - geomParameters.fillThickness)/4.0f - geomParameters.fillThickness/2.0f, 0.0f),fillerLog2,"Filler2B",detectorLog,false,1);

    
    // Housing
    // Housing base
    auto housingBaseBox = new G4Box("HousingBase", geomParameters.housingWidth/2.0f, geomParameters.housingWidth/2.0f, geomParameters.housingBaseThickness/2.0f);
    auto housingBaseLog = new G4LogicalVolume(housingBaseBox,PLA,"HousingBase",0,0,0);
    auto housingBaseBoxVA = new G4VisAttributes(G4Colour(1.0, 0.7, 0.0));
    housingBaseLog->SetVisAttributes(housingBaseBoxVA);
    G4double baseZOffset = geomParameters.scintLength/2.0f + geomParameters.sipmThickness + geomParameters.boardThickness + geomParameters.housingBaseThickness/2.0f; 
    auto housingBasePhys2A = new G4PVPlacement(0,G4ThreeVector(0, 0, -baseZOffset),housingBaseLog,"HousingBaseA",detectorLog,false,0);
    auto housingBasePhys2B = new G4PVPlacement(rotY180,G4ThreeVector(0, 0, baseZOffset),housingBaseLog,"HousingBaseB",detectorLog,false,1);
    
    //Housing walls
    auto housingWallOuterBox = new G4Box("HousingOuter", geomParameters.housingWidth/2.0f, geomParameters.housingWidth/2.0f, geomParameters.scintLength/2.0f + geomParameters.sipmThickness + geomParameters.boardThickness);
    auto housingWallInnerBox = new G4Box("HousingInner", geomParameters.housingWidth/2.0f - geomParameters.housingWallThickness, geomParameters.housingWidth/2.0f - geomParameters.housingWallThickness, geomParameters.scintLength/2.0f + geomParameters.sipmThickness + geomParameters.boardThickness);
    auto housingWalls = new G4SubtractionSolid ("HousingWalls", housingWallOuterBox, housingWallInnerBox);
    auto housingWallsLog = new G4LogicalVolume(housingWalls,PLA,"HousingWalls",0,0,0);
    housingWallsLog->SetVisAttributes(housingBaseBoxVA);
    auto housingWallsPhys = new G4PVPlacement(0,G4ThreeVector(),housingWallsLog,"HousingWalls",detectorLog,false,0);

    //PCB
    auto pcbBox = new G4Box("PCB", geomParameters.boardWidth/2.0f, geomParameters.boardWidth/2.0f, geomParameters.boardThickness/2.0f);  
    auto pcbLog = new G4LogicalVolume(pcbBox,PCB,"PCB",0,0,0);
    auto pcbBoxVA = new G4VisAttributes(G4Colour(0.2,0.8,0.2));
    pcbLog->SetVisAttributes(pcbBoxVA);
    G4double pcbXYOffset = geomParameters.housingWidth/2.0f - geomParameters.housingWallThickness - geomParameters.boardWidth/2.0f;
    G4double pcbZOffset = geomParameters.boardThickness/2.0f + geomParameters.housingBaseThickness/2.0f; 
    auto pcbPhysA = new G4PVPlacement(0,G4ThreeVector(-pcbXYOffset, -pcbXYOffset, pcbZOffset),pcbLog,"PCBA",housingBaseLog,false,0);
    auto pcbPhysB = new G4PVPlacement(0,G4ThreeVector(-pcbXYOffset, pcbXYOffset, pcbZOffset),pcbLog,"PCBA",housingBaseLog,false,1);
    auto pcbPhysC = new G4PVPlacement(0,G4ThreeVector(pcbXYOffset, pcbXYOffset, pcbZOffset),pcbLog,"PCBA",housingBaseLog,false,2);
    auto pcbPhysD = new G4PVPlacement(0,G4ThreeVector(pcbXYOffset, -pcbXYOffset, pcbZOffset),pcbLog,"PCBA",housingBaseLog,false,3);
    
    G4double wrapThickness = 0.635*mm;
    G4double wrappingWidth = geomParameters.scintWidth + 2 * geomParameters.wrapThickness;
    
     // Wrapping
    auto wrapBox = new G4Box("Wrapping", wrappingWidth/2.0f, wrappingWidth/2.0f, geomParameters.scintLength/2.0f);
    auto wrapLog = new G4LogicalVolume(wrapBox,Pb,"Wrapping",0,0,0);
    auto wrapVA = new G4VisAttributes(G4Colour(0.5,0.5,0.5));
    wrapLog->SetVisAttributes(wrapVA);
    G4double wrappingOffset=geomParameters.sipmSeparation/2.0f;

    auto wrapPhysRT = new G4PVPlacement(rotZ180,G4ThreeVector(wrappingOffset, wrappingOffset, 0),wrapLog,"WrappingRT",detectorLog,false,2);
    if (!scintParameters.singleScint)
    {
        auto wrapPhysLB = new G4PVPlacement(0,G4ThreeVector(-wrappingOffset, -wrappingOffset, 0),wrapLog,"WrappingLB",detectorLog,false,0);
        auto wrapPhysLT = new G4PVPlacement(rotZ90,G4ThreeVector(-wrappingOffset, wrappingOffset, 0),wrapLog,"WrappingLT",detectorLog,false,1);
        auto wrapPhysRB = new G4PVPlacement(rotZ270,G4ThreeVector(wrappingOffset, -wrappingOffset, 0),wrapLog,"WrappingRB",detectorLog,false,3);
    }
    
    // Scintillator
    auto scintBox = new G4Box("Scint", geomParameters.scintWidth/2.0f, geomParameters.scintWidth/2.0f, geomParameters.scintLength/2.0f);
    auto scintLog = new G4LogicalVolume(scintBox,PVT_ej299,"scintLog",0,0,0);
    auto scintVA = new G4VisAttributes(G4Colour(0.5,0.5,1.0));
    scintLog->SetVisAttributes(scintVA);
    auto scintPhys = new G4PVPlacement(0,G4ThreeVector(0,0,0),scintLog,"Scint", wrapLog,false,0);

    //**Scintillator housing properties
    const G4int nEntriesOther = 2;
    G4double PhotonEnergyOther[nEntriesOther] = { 2*eV,4*eV };
    G4double ReflectivityWrapping[nEntriesOther] = {scintParameters.coatingReflectivity, scintParameters.coatingReflectivity};
    G4double EfficiencyWrapping[nEntriesOther] = {0.0, 0.0};
    G4MaterialPropertiesTable* ptWrapping = new G4MaterialPropertiesTable();
    ptWrapping->AddProperty("REFLECTIVITY", PhotonEnergyOther, ReflectivityWrapping, nEntriesOther);
    ptWrapping->AddProperty("EFFICIENCY", PhotonEnergyOther, EfficiencyWrapping, nEntriesOther);
	G4OpticalSurface* OpScintHousingSurface = new G4OpticalSurface("WrappingSurface", unified, polishedfrontpainted, dielectric_dielectric);
    OpScintHousingSurface->SetMaterialPropertiesTable(ptWrapping);
    new G4LogicalSkinSurface("scint_surf",wrapLog,OpScintHousingSurface);
    
    //Photon detector    
    auto photonDetectorBox = new G4Box("Scint", geomParameters.scintWidth/2.0f, geomParameters.scintWidth/2.0f, geomParameters.sipmThickness/2.0f);
    photonDetectorLog = new G4LogicalVolume(photonDetectorBox,Silicon,"photonDetectorLog",0,0,0);
    auto photonDetectorVA = new G4VisAttributes(G4Colour(0.7,0.7,1.0));
    photonDetectorLog->SetVisAttributes(photonDetectorVA);

    G4double zOffsetSiPM=geomParameters.scintLength/2.0+geomParameters.sipmThickness/2.0;
    G4double xyOffsetSiPM=geomParameters.sipmSeparation/2.0;
    //Front SiPMs
    photonDetectorPhys[2]=new G4PVPlacement(0,G4ThreeVector( xyOffsetSiPM, xyOffsetSiPM,-zOffsetSiPM),photonDetectorLog,"SiPM_RT_Front", detectorLog,false,2);
    if (!scintParameters.singleScint)
    {
        photonDetectorPhys[0]=new G4PVPlacement(0,G4ThreeVector(-xyOffsetSiPM,-xyOffsetSiPM,-zOffsetSiPM),photonDetectorLog,"SiPM_LB_Front", detectorLog,false,0);
        photonDetectorPhys[1]=new G4PVPlacement(0,G4ThreeVector(-xyOffsetSiPM, xyOffsetSiPM,-zOffsetSiPM),photonDetectorLog,"SiPM_LT_Front", detectorLog,false,1);
        photonDetectorPhys[3]=new G4PVPlacement(0,G4ThreeVector( xyOffsetSiPM,-xyOffsetSiPM,-zOffsetSiPM),photonDetectorLog,"SiPM_RB_Front", detectorLog,false,3);
    }
    //Back SiPMs    
    photonDetectorPhys[6]=new G4PVPlacement(rotY180,G4ThreeVector( xyOffsetSiPM, xyOffsetSiPM,zOffsetSiPM),photonDetectorLog,"SiPM_RT_Back", detectorLog,false,6);
    if (!scintParameters.singleScint)
    {
        photonDetectorPhys[4]=new G4PVPlacement(rotY180,G4ThreeVector(-xyOffsetSiPM,-xyOffsetSiPM,zOffsetSiPM),photonDetectorLog,"SiPM_LB_Back", detectorLog,false,4);
        photonDetectorPhys[5]=new G4PVPlacement(rotY180,G4ThreeVector(-xyOffsetSiPM, xyOffsetSiPM,zOffsetSiPM),photonDetectorLog,"SiPM_LT_Back", detectorLog,false,5);
        photonDetectorPhys[7]=new G4PVPlacement(rotY180,G4ThreeVector( xyOffsetSiPM,-xyOffsetSiPM,zOffsetSiPM),photonDetectorLog,"SiPM_RB_Back", detectorLog,false,7);
    }
    
#pragma endregion Volumes

#pragma region material props
    //
    // Material Properties (ej299)
    //
    const G4int nEntriesEJ299 = 18;
    G4double PhotonEnergyEJ299[nEntriesEJ299] =
    { 2.30*eV, 2.34*eV, 2.47*eV, 2.58*eV,
      2.63*eV, 2.72*eV, 2.75*eV, 2.79*eV,
      2.88*eV, 2.95*eV, 3.02*eV, 3.06*eV,
      3.10*eV, 3.14*eV, 3.17*eV, 3.22*eV,
      3.26*eV, 3.31*eV};


    //
    // ej299
    //

    //assume const. refractive index and effectively no absoprtion
    G4double RefractiveIndexEJ299[nEntriesEJ299] =
    { 1.58, 1.58, 1.58, 1.58,
      1.58, 1.58, 1.58, 1.58,
      1.58, 1.58, 1.58, 1.58,
      1.58, 1.58, 1.58, 1.58,
      1.58, 1.58};

    G4double AbsorptionEJ299[nEntriesEJ299] =
    { 1*m, 1*m, 1*m, 1*m,
      1*m, 1*m, 1*m, 1*m,
      1*m, 1*m, 1*m, 1*m,
      1*m, 1*m, 1*m, 1*m,
      1*m, 1*m};

    G4double ScintilFastEJ299[nEntriesEJ299] =
    { 0.00, 0.10, 0.20, 0.35,
      0.40, 0.60, 0.63, 0.65,
      0.80, 1.00, 0.80, 0.70,
      0.67, 0.60, 0.40, 0.20,
      0.10, 0.00};

    G4double ScintilSlowEJ299[nEntriesEJ299] =
    { 0.00, 0.10/10, 0.20/10, 0.35/10,
      0.40/10, 0.60/10, 0.63/10, 0.65/10,
      0.80/10, 1.00/10, 0.80/10, 0.70/10,
      0.67/10, 0.60/10, 0.40/10, 0.20/10,
      0.10/10, 0.00};

    G4MaterialPropertiesTable* ptEJ299 = new G4MaterialPropertiesTable();
    ptEJ299->AddProperty("RINDEX", PhotonEnergyEJ299, RefractiveIndexEJ299, nEntriesEJ299)->SetSpline(true);
    ptEJ299->AddProperty("ABSLENGTH", PhotonEnergyEJ299, AbsorptionEJ299, nEntriesEJ299)->SetSpline(true);
    ptEJ299->AddProperty("FASTCOMPONENT",PhotonEnergyEJ299, ScintilFastEJ299, nEntriesEJ299)->SetSpline(true);
    ptEJ299->AddProperty("SLOWCOMPONENT",PhotonEnergyEJ299, ScintilSlowEJ299, nEntriesEJ299)->SetSpline(true);

    G4int numEntriesLightYield = 20;
    G4double yieldElectron[20];
    G4double yieldProton[20];
    G4double energies [20] =   {1.0*keV, 2.0*keV, 5.0*keV,
                                10.*keV, 20.*keV, 50.*keV,
                                100*keV, 200*keV, 500*keV,
                                1.0*MeV, 2.0*MeV, 3.0*MeV,
                                4.0*MeV, 5.0*MeV, 6.0*MeV,
                                8.0*MeV, 9.0*MeV, 10.*MeV, 
                                12.*MeV, 15.0*MeV};

    for (int i=0;i<numEntriesLightYield;i++)
    {
        //From "Neutron response characterization for an EJ299-33 plastic scintillation detector"
        //Lawrence et al, NIMA
        G4double E=energies[i]/MeV;
        G4double a=7.5e-1, b=3.2, c=2.2e-1;
        yieldElectron[i]=scintParameters.photonYield*E;
        yieldProton[i]=scintParameters.photonYield*(a*E-b*(1-exp(-c*E)));
    }

    ptEJ299->AddProperty("ELECTRONSCINTILLATIONYIELD",energies, yieldElectron, numEntriesLightYield)->SetSpline(true);
    ptEJ299->AddProperty("PROTONSCINTILLATIONYIELD",energies, yieldProton, numEntriesLightYield)->SetSpline(true);
    //ptEJ299->AddProperty("IONSCINTILLATIONYIELD",energies, yieldCarbon, 2)->SetSpline(true);
    ptEJ299->AddConstProperty("RESOLUTIONSCALE",scintParameters.resolution);
    ptEJ299->AddConstProperty("FASTTIMECONSTANT", 7.4*ns);
    //ptEJ299->AddConstProperty("SLOWTIMECONSTANT", 95.0*ns);
    ptEJ299->AddConstProperty("SLOWTIMECONSTANT", 127.0*ns);
    ptEJ299->AddConstProperty("ELECTRONSCINTILLATIONYIELDRATIO",0.72);    
    //ptEJ299->AddConstProperty("PROTONSCINTILLATIONYIELDRATIO",1.0-0.0443);
    ptEJ299->AddConstProperty("PROTONSCINTILLATIONYIELDRATIO",0.65);
    //ptEJ299->AddConstProperty("IONSCINTILLATIONYIELDRATIO",0.3);
    //ptEJ299->AddConstProperty("ALPHASCINTILLATIONYIELDRATIO",0.1);
    PVT_ej299->SetMaterialPropertiesTable(ptEJ299);

    //
    // Material Properties (Ne102a)
    //
    const G4int nEntriesNE102a = 13;
    G4double PhotonEnergyNE102a[nEntriesNE102a] =
    { 2.38*eV, 2.48*eV, 2.58*eV, 2.70*eV,
      2.71*eV, 2.77*eV, 2.82*eV, 2.85*eV,
      2.88*eV, 2.93*eV, 2.95*eV, 3.02*eV,
      3.10*eV };


    //
    // NE102a
    //
    G4double RefractiveIndexNE102a[nEntriesNE102a] =
    { 1.58, 1.58, 1.58, 1.58,
      1.58, 1.58, 1.58, 1.58,
      1.58, 1.58, 1.58, 1.58,
      1.58 };

    G4double AbsorptionNE102a[nEntriesNE102a] =
    { 1*m, 1*m, 1*m, 1*m,
      1*m, 1*m, 1*m, 1*m,
      1*m, 1*m, 1*m, 1*m,
      1*m };

    G4double ScintilFastNE102a[nEntriesNE102a] =
    { 0.08, 0.12, 0.21, 0.37,
      0.40, 0.60, 0.70, 0.72,
      0.80, 1.00, 0.95, 0.15,
      0.07 };

    G4MaterialPropertiesTable* ptNE102a = new G4MaterialPropertiesTable();
    ptNE102a->AddProperty("RINDEX", PhotonEnergyNE102a, RefractiveIndexNE102a, nEntriesNE102a)->SetSpline(true);
    ptNE102a->AddProperty("ABSLENGTH", PhotonEnergyNE102a, AbsorptionNE102a, nEntriesNE102a)->SetSpline(true);
    ptNE102a->AddProperty("FASTCOMPONENT",PhotonEnergyNE102a, ScintilFastNE102a, nEntriesNE102a)->SetSpline(true);
    ptNE102a->AddProperty("ELECTRONSCINTILLATIONYIELD",energies, yieldElectron, numEntriesLightYield)->SetSpline(true);
    ptNE102a->AddProperty("PROTONSCINTILLATIONYIELD",energies, yieldProton, 2)->SetSpline(true);
    //ptNE102a->AddProperty("CARBONSCINTILLATIONYIELD",energies, yieldCarbon, 2)->SetSpline(true);
    //ptNE102a->AddConstProperty("SCINTILLATIONYIELD",scintYield/MeV);

    ptNE102a->AddConstProperty("SCINTILLATIONYIELD",scintParameters.photonYield/MeV);
    ptNE102a->AddConstProperty("RESOLUTIONSCALE",1.0);
    ptNE102a->AddConstProperty("FASTTIMECONSTANT", 2.4*ns);
    ptNE102a->AddConstProperty("YIELDRATIO",1.0);
    PVT_ne102->SetMaterialPropertiesTable(ptNE102a);

    //TODO: calculate Birks for PVT
    PVT_ne102->GetIonisation()->SetBirksConstant(0.126*mm/MeV);

    //
    // Air
    //
    G4double RefractiveIndexAir[nEntriesOther] =
    { 1.00, 1.00};

    G4MaterialPropertiesTable* ptAir = new G4MaterialPropertiesTable();
    ptAir->AddProperty("RINDEX", PhotonEnergyOther, RefractiveIndexAir, nEntriesOther);
    Air->SetMaterialPropertiesTable(ptAir);


    G4double RefractiveIndexSi[nEntriesOther]={1.49,1.49};
    G4double AbsorptionSi[nEntriesOther]={420.*cm,420.*cm};
    G4MaterialPropertiesTable *ptSi = new G4MaterialPropertiesTable();
    ptSi->AddProperty("ABSLENGTH",PhotonEnergyOther,AbsorptionSi,nEntriesOther);
    ptSi->AddProperty("RINDEX",PhotonEnergyOther,RefractiveIndexSi,nEntriesOther);
    Silicon->SetMaterialPropertiesTable(ptSi);

    G4double RefractiveIndexGlass[nEntriesOther]={1.50,1.50};
    G4double AbsorptionGlass[nEntriesOther]={420.*cm,420.*cm};
    G4MaterialPropertiesTable *ptGlass = new G4MaterialPropertiesTable();
    ptGlass->AddProperty("ABSLENGTH",PhotonEnergyOther,AbsorptionGlass,nEntriesOther);
    ptGlass->AddProperty("RINDEX",PhotonEnergyOther,RefractiveIndexGlass,nEntriesOther);
    Glass->SetMaterialPropertiesTable(ptGlass);

#pragma endregion material props

#pragma region surfaces
    // ------------- Surfaces --------------
    //
    // Scint
    //
    G4OpticalSurface* OpScintSurface = new G4OpticalSurface("ScintSurface");
    OpScintSurface->SetType(dielectric_dielectric);
    OpScintSurface->SetFinish(polished);
    OpScintSurface->SetModel(glisur);
    //G4LogicalBorderSurface* ScintSurface = new G4LogicalBorderSurface("ScintSurface", scint_phys,expHall_phys,OpScintSurface);
    OpScintSurface->SetMaterialPropertiesTable(ptAir);

    //for now, assume constant efficiency (35%)
    G4double pmt_EFF[nEntriesOther]={photonParameters.efficiency,photonParameters.efficiency};
    //need to check reflectivity of pmt. And where does the refractive index come from??
    G4double pmt_REF[nEntriesOther]={photonParameters.reflectivity,photonParameters.reflectivity};
    G4double pmt_ReR[nEntriesOther]={5.,5.};
    G4double pmt_ImR[nEntriesOther]={0.2,0.2};
    G4MaterialPropertiesTable* photonDetectorMT = new G4MaterialPropertiesTable();
    photonDetectorMT->AddProperty("EFFICIENCY",PhotonEnergyOther,pmt_EFF,nEntriesOther);
    photonDetectorMT->AddProperty("REFLECTIVITY",PhotonEnergyOther,pmt_REF,nEntriesOther);
    photonDetectorMT->AddProperty("REALRINDEX",PhotonEnergyOther,pmt_ReR,nEntriesOther);
    photonDetectorMT->AddProperty("IMAGINARYRINDEX",PhotonEnergyOther,pmt_ImR,nEntriesOther);
    G4OpticalSurface* photonDetectorOpSurf= new G4OpticalSurface("pmtOpSurf",glisur,polished,dielectric_metal);
    photonDetectorOpSurf->SetMaterialPropertiesTable(photonDetectorMT);
    Silicon->SetMaterialPropertiesTable(photonDetectorMT);
    G4LogicalSkinSurface* photonDetectorSurf=new G4LogicalSkinSurface("pmtSurf",photonDetectorLog,photonDetectorOpSurf);
    
#pragma endregion surface
    //return the parent physical volume
    return expHallPhys;
        
}

void OpticalSimDetectorConstruction::ConstructSDandField()
{
	//G4SDManager* SDman = G4SDManager::GetSDMpointer();
    OpticalSimPMTSensitiveDetector* fPMTSD = new OpticalSimPMTSensitiveDetector("/PMTSD");
	SetSensitiveDetector("photonDetectorLog", fPMTSD);
	OpticalSimScintSensitiveDetector* fScintSD = new OpticalSimScintSensitiveDetector("/ScintSD");
	SetSensitiveDetector("scintLog", fScintSD);


}
