#include "G4ios.hh"
#include "G4VVisManager.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"

#include "OpticalSimScintHit.hh"

G4Allocator<OpticalSimScintHit> OpticalSimScintHitAllocator;

OpticalSimScintHit::OpticalSimScintHit()
	: physVol(0), drawFlag(false), scintCopyNumber(-1), eDep(0), timeFirstHit(-1)
{

}

OpticalSimScintHit::~OpticalSimScintHit()
{

}

OpticalSimScintHit::OpticalSimScintHit(const OpticalSimScintHit &right) : G4VHit()
{
	eDep = right.eDep;
	physVol = right.physVol;
	scintCopyNumber = right.scintCopyNumber;
	timeFirstHit = right.timeFirstHit;
	drawFlag = right.drawFlag;

}

const OpticalSimScintHit& OpticalSimScintHit::operator=(const OpticalSimScintHit &right)
{
	eDep = right.eDep;
	physVol = right.physVol;
	scintCopyNumber = right.scintCopyNumber;
	timeFirstHit = right.timeFirstHit;
	drawFlag = right.drawFlag;
	return *this;
}

G4int OpticalSimScintHit::operator==(const OpticalSimScintHit &right) const
{
	return (scintCopyNumber == right.scintCopyNumber);
}

void OpticalSimScintHit::Draw()
{
	if (drawFlag && physVol)
	{
		G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
		if (pVVisManager)
		{
			//draw in red
			G4VisAttributes attribs(G4Colour(1., 0., 0.));
			attribs.SetForceSolid(true);
			G4RotationMatrix rot;
			if (physVol->GetRotation())//If a rotation is defined use it
				rot = *(physVol->GetRotation());
			G4Transform3D trans(rot, physVol->GetTranslation());//Create transform
			pVVisManager->Draw(*physVol, attribs, trans);//Draw it
		}
	}
}