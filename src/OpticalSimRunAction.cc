#include "G4Timer.hh"
#include "G4Run.hh"
#include "G4SystemOfUnits.hh"
#include "Analysis.hh"
#include "OpticalSimRunAction.hh"
#include <stdio.h>

OpticalSimRunAction::OpticalSimRunAction(G4int s_seed, G4int s_numberOfEvents, DetectorGeometryParameters s_geomParameters, ScintillatorParameters s_scintParameters, PhotonDetectorParameters s_photonParameters)
	:seed(s_seed), numberOfEvents(s_numberOfEvents), geomParameters(s_geomParameters), scintParameters(s_scintParameters), photonParameters(s_photonParameters)
{
  timer = new G4Timer;
  // Create analysis manager
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->SetVerboseLevel(1);
  analysisManager->SetCompressionLevel(6);
  char seedVal[255];
  sprintf(seedVal, "QuadScint_%i", seed);
  analysisManager->SetFileName(seedVal);
  analysisManager->SetNtupleDirectoryName("ntuple");
  analysisManager->SetHistoDirectoryName("hist");
  analysisManager->SetFirstHistoId(1);
  analysisManager->SetFirstNtupleId(1);
  // Creating histograms
  analysisManager->CreateH1("1", "Total Energy Deposited LB Cell", 256, 0., 10 * MeV);
  analysisManager->CreateH1("2", "Total Photons Detected", 256, 0., 10000);

  //Create tuples for run info id=1
  analysisManager->CreateNtuple("RunInfo", "Run info");
  analysisManager->CreateNtupleIColumn("Seed");
  analysisManager->CreateNtupleIColumn("NumEvents");
  analysisManager->CreateNtupleIColumn("RunVersion");
  analysisManager->FinishNtuple(1);

  //Create tuples for geom id=2
  analysisManager->CreateNtuple("GeomParms", "Geometry Parameters");
  analysisManager->CreateNtupleDColumn("ScintWidth");
  analysisManager->CreateNtupleDColumn("ScintLength");
  analysisManager->CreateNtupleDColumn("WrapThickness");
  analysisManager->CreateNtupleDColumn("FillThickness");
  analysisManager->CreateNtupleDColumn("FillWidth");
  analysisManager->CreateNtupleDColumn("FillLength");
  analysisManager->CreateNtupleDColumn("SiPMThickness");
  analysisManager->CreateNtupleDColumn("SiPMSeparation");
  analysisManager->CreateNtupleDColumn("BoardThickness");
  analysisManager->CreateNtupleDColumn("BoardWidth");
  analysisManager->CreateNtupleDColumn("HousingLength");
  analysisManager->CreateNtupleDColumn("HousingWidth");
  analysisManager->CreateNtupleDColumn("HousingWallThickness");
  analysisManager->CreateNtupleDColumn("HousingSpacerThickness");
  analysisManager->CreateNtupleDColumn("HousingBaseThickness");
  analysisManager->CreateNtupleDColumn("HousingPLADensityFactor");
  analysisManager->CreateNtupleDColumn("ZRotationDeg");
  analysisManager->FinishNtuple(2);

  //Create tuples for scint id=3
  analysisManager->CreateNtuple("ScintParams", "Scintillation Material Parameters");
  analysisManager->CreateNtupleDColumn("PhotonYield");
  analysisManager->CreateNtupleDColumn("Resolution");
  analysisManager->CreateNtupleDColumn("CoatingReflectivity");
  analysisManager->FinishNtuple(3);

  //Create tuples for pd id=4
  analysisManager->CreateNtuple("PhotonParams", "Photon Detector Parameters");
  analysisManager->CreateNtupleDColumn("Reflectivity");
  analysisManager->CreateNtupleDColumn("Efficiency");
  analysisManager->FinishNtuple(4);

  //Create tuples for hit counts id=5
  analysisManager->CreateNtuple("EnergyDeps", "Energy Deposits for each scint");
 // analysisManager->CreateNtupleIColumn("EventNum");
  //Energy Deposited (MC truth)
  analysisManager->CreateNtupleDColumn("Edep_LB");
  analysisManager->CreateNtupleDColumn("Edep_LT");
  analysisManager->CreateNtupleDColumn("Edep_RT");
  analysisManager->CreateNtupleDColumn("Edep_RB");
  analysisManager->CreateNtupleDColumn("time_LB");
  analysisManager->CreateNtupleDColumn("time_LT");
  analysisManager->CreateNtupleDColumn("time_RT");
  analysisManager->CreateNtupleDColumn("time_RB");
  //Photons Detected by SiPMs
  analysisManager->CreateNtupleIColumn("LB_Front");
  analysisManager->CreateNtupleIColumn("LT_Front");
  analysisManager->CreateNtupleIColumn("RT_Front");
  analysisManager->CreateNtupleIColumn("RB_Front");
  analysisManager->CreateNtupleIColumn("LB_Back");
  analysisManager->CreateNtupleIColumn("LT_Back");
  analysisManager->CreateNtupleIColumn("RT_Back");
  analysisManager->CreateNtupleIColumn("RB_Back");
  //Photons Detected within PSD Cut time
//   analysisManager->CreateNtupleIColumn("PSD_LB_Front");
//   analysisManager->CreateNtupleIColumn("PSD_LT_Front");
//   analysisManager->CreateNtupleIColumn("PSD_RT_Front");
//   analysisManager->CreateNtupleIColumn("PSD_RB_Front");
//   analysisManager->CreateNtupleIColumn("PSD_LB_Back");
//   analysisManager->CreateNtupleIColumn("PSD_LT_Back");
//   analysisManager->CreateNtupleIColumn("PSD_RT_Back");
//   analysisManager->CreateNtupleIColumn("PSD_RB_Back");
  analysisManager->FinishNtuple(5);
}

void OpticalSimRunAction::fillRunInfoTuples()
{
	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
	//write run info data (nTuple id=1)
	analysisManager->FillNtupleIColumn(1, 0, seed);
	analysisManager->FillNtupleIColumn(1, 1, numberOfEvents);
	analysisManager->FillNtupleIColumn(1, 2, ANALYSIS_OUTPUT_VERSION);
	analysisManager->AddNtupleRow(1);
	//write geom info (nTuple id=2)
	analysisManager->FillNtupleDColumn(2, 0, geomParameters.scintWidth);
	analysisManager->FillNtupleDColumn(2, 1, geomParameters.scintLength);
	analysisManager->FillNtupleDColumn(2, 2, geomParameters.wrapThickness);
	analysisManager->FillNtupleDColumn(2, 3, geomParameters.fillThickness);
	analysisManager->FillNtupleDColumn(2, 4, geomParameters.fillWidth);
	analysisManager->FillNtupleDColumn(2, 5, geomParameters.fillLength);
	analysisManager->FillNtupleDColumn(2, 6, geomParameters.sipmThickness);
	analysisManager->FillNtupleDColumn(2, 7, geomParameters.sipmSeparation);
	analysisManager->FillNtupleDColumn(2, 8, geomParameters.boardThickness);
	analysisManager->FillNtupleDColumn(2, 9, geomParameters.boardWidth);
	analysisManager->FillNtupleDColumn(2, 10, geomParameters.housingLength);
	analysisManager->FillNtupleDColumn(2, 11, geomParameters.housingWidth);
	analysisManager->FillNtupleDColumn(2, 12, geomParameters.housingWallThickness);
	analysisManager->FillNtupleDColumn(2, 13, geomParameters.housingSpacerThickness);
	analysisManager->FillNtupleDColumn(2, 14, geomParameters.housingBaseThickness);
	analysisManager->FillNtupleDColumn(2, 15, geomParameters.housingPLADensityFactor);
	analysisManager->FillNtupleDColumn(2, 16, geomParameters.zRotationDeg);
	analysisManager->AddNtupleRow(2);
	//write scint info (nTuple id=3)
	analysisManager->FillNtupleDColumn(3, 0, scintParameters.photonYield);
	analysisManager->FillNtupleDColumn(3, 1, scintParameters.resolution);
	analysisManager->FillNtupleDColumn(3, 2, scintParameters.coatingReflectivity);
	analysisManager->AddNtupleRow(3);
	//write pd info (nTuple id=4)
	analysisManager->FillNtupleDColumn(4, 0, photonParameters.reflectivity);
	analysisManager->FillNtupleDColumn(4, 1, photonParameters.efficiency);
	analysisManager->AddNtupleRow(4);

}

OpticalSimRunAction::~OpticalSimRunAction()
{
  delete timer;
  delete G4AnalysisManager::Instance();
}

void OpticalSimRunAction::BeginOfRunAction(const G4Run* aRun)
{
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl; 
  timer->Start();
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  // Open an output file
  analysisManager->OpenFile();
  fillRunInfoTuples();
}

void OpticalSimRunAction::EndOfRunAction(const G4Run* aRun)
{   
	timer->Stop();
	G4cout << "number of event = " << aRun->GetNumberOfEvent() 
			<< " " << *timer << G4endl;
	// Save histograms
	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
	analysisManager->Write();
	analysisManager->CloseFile();
}
