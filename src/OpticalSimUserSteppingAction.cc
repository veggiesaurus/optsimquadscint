#include "G4ProcessManager.hh"
#include "G4SteppingManager.hh"
#include "G4SDManager.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4Event.hh"
#include "G4StepPoint.hh"
#include "G4TrackStatus.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4OpBoundaryProcess.hh"

#include "OpticalSimUserSteppingAction.hh"
#include "OpticalSimPMTSensitiveDetector.hh"
#include "OpticalSimScintSensitiveDetector.hh"

OpticalSimUserSteppingAction::OpticalSimUserSteppingAction()
{

}

OpticalSimUserSteppingAction::~OpticalSimUserSteppingAction(){}


void OpticalSimUserSteppingAction::UserSteppingAction(const G4Step* theStep)
{
    G4Track* theTrack = theStep->GetTrack();
    G4ParticleDefinition* particleType = theTrack->GetDefinition();
    G4StepPoint* thePrePoint = theStep->GetPreStepPoint();
    G4VPhysicalVolume* thePrePV = thePrePoint->GetPhysicalVolume();

    //process p, e+, e-
    /*if (particleType == G4Proton::ProtonDefinition() || particleType == G4Positron::PositronDefinition() || particleType == G4Electron::ElectronDefinition())
    {
        G4String name=thePrePV->GetName();        
        name.toLower();
        if(name.contains("scint"))
        {
            G4SDManager* SDman = G4SDManager::GetSDMpointer();
            OpticalSimScintSensitiveDetector* sd = (OpticalSimScintSensitiveDetector*)SDman->FindSensitiveDetector("/ScintSD");
            if(sd)
            {
                sd->ProcessHits_constStep(theStep,NULL);
            }
        }
    }*/


    G4StepPoint* thePostPoint = theStep->GetPostStepPoint();
    G4VPhysicalVolume* thePostPV = thePostPoint->GetPhysicalVolume();

    G4OpBoundaryProcessStatus boundaryStatus=Undefined;
    static G4OpBoundaryProcess* boundary=NULL;

    //find the boundary process only once
    if(!boundary)
    {
        G4ProcessManager* pm = theStep->GetTrack()->GetDefinition()->GetProcessManager();
        G4int nprocesses = pm->GetProcessListLength();
        G4ProcessVector* pv = pm->GetProcessList();
        G4int i;
        for( i=0;i<nprocesses;i++){
            if((*pv)[i]->GetProcessName()=="OpBoundary"){
                boundary = (G4OpBoundaryProcess*)(*pv)[i];
                break;
            }
        }
    }

    //kill tracks with no next volume
    if(!thePostPV)
        return;

    G4String postVolName=thePostPV->GetName();
    G4int postVolCopyNo=thePostPV->GetCopyNo();


    //only record optical photons
    if(particleType==G4OpticalPhoton::OpticalPhotonDefinition())
    {
        if(postVolName=="expHall")
            theTrack->SetTrackStatus(fStopAndKill);
        boundaryStatus=boundary->GetStatus();
        if(thePostPoint->GetStepStatus()==fGeomBoundary)
        {
            switch(boundaryStatus)
            {
            case Absorption:
                //if(thePostPV->GetName()=="PMT_front" || thePostPV->GetName()=="PMT_back")
                //    G4cout<<"Photon absorbed in "<<postVolName<<"["<<postVolCopyNo<<"]"<<G4endl;
                //else
                //    G4cout<<"Photon absorbed in "<<postVolName<<G4endl;
                break;
            case Detection:
            {
                if(thePostPV->GetName().contains("SiPM"))
                {					
                    G4SDManager* SDman = G4SDManager::GetSDMpointer();
                    OpticalSimPMTSensitiveDetector* sd = (OpticalSimPMTSensitiveDetector*)SDman->FindSensitiveDetector("/PMTSD");
                    if(sd)
                    {
                        sd->ProcessHits_constStep(theStep,NULL);
                        theTrack->SetTrackStatus(fStopAndKill);
                    }
                    else
                        theTrack->SetTrackStatus(fStopAndKill);
                }
                break;
            }
            default:
                break;
            }
        }
    }
}
