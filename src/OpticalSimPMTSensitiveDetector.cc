#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4ios.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"

#include "OpticalSimPMTSensitiveDetector.hh"
#include "OpticalSimPMTHit.hh"

OpticalSimPMTSensitiveDetector::OpticalSimPMTSensitiveDetector(G4String name)
    : G4VSensitiveDetector(name),PMTHitCollection(0)
{
    collectionName.insert("PMTHitCollection");
}

OpticalSimPMTSensitiveDetector::~OpticalSimPMTSensitiveDetector()
{

}

void OpticalSimPMTSensitiveDetector::Initialize(G4HCofThisEvent* hitsCE)
{
    PMTHitCollection = new OpticalSimPMTHitsCollection(SensitiveDetectorName,collectionName[0]);
    //Store collection with event and keep ID
    static G4int hitCID = -1;
    if(hitCID<0){
        hitCID = GetCollectionID(0);
    }
    hitsCE->AddHitsCollection( hitCID, PMTHitCollection );
}

G4bool OpticalSimPMTSensitiveDetector::ProcessHits(G4Step* ,G4TouchableHistory* )
{
    return false;
}

G4bool OpticalSimPMTSensitiveDetector::ProcessHits_constStep(const G4Step* aStep,
                                                             G4TouchableHistory* )
{
    //need to know if this is an optical photon
    if(aStep->GetTrack()->GetDefinition()!= G4OpticalPhoton::OpticalPhotonDefinition())
        return false;

	//User replica number 1 since photocathode is a daughter volume
    //to the PMT which was replicated

    G4VPhysicalVolume* physVol= aStep->GetPostStepPoint()->GetPhysicalVolume();
    G4int pdCopyNumber= physVol->GetCopyNo();

    //Find the correct hit collection
	G4int n = PMTHitCollection->entries();
	OpticalSimPMTHit* hit = NULL;
	for (G4int i = 0; i < n; i++)
	{
		if ((*PMTHitCollection)[i]->GetPDCopyNumber() == pdCopyNumber)
		{
			hit = (*PMTHitCollection)[i];
			break;
		}
	}

	//this pd wasnt previously hit in this event
	if (hit == NULL)
	{
		hit = new OpticalSimPMTHit(); //so create new hit
		hit->SetPMTPhysVol(physVol);
		hit->SetPDCopyNumber(pdCopyNumber);
		PMTHitCollection->insert(hit);
	}

    G4double hitTime=aStep->GetTrack()->GetGlobalTime();
    hit->AddPhoton(hitTime);
    hit->SetDrawFlag(true);
    return true;
}

void OpticalSimPMTSensitiveDetector::EndOfEvent(G4HCofThisEvent* )
{

}

void OpticalSimPMTSensitiveDetector::clear()
{

}

void OpticalSimPMTSensitiveDetector::DrawAll()
{

}

void OpticalSimPMTSensitiveDetector::PrintAll()
{

}
