#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4GeneralParticleSource.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "G4String.hh"

#include "Randomize.hh"
#include "OpticalSimPrimaryGeneratorAction.hh"

OpticalSimPrimaryGeneratorAction::OpticalSimPrimaryGeneratorAction()
{  
  source = new G4GeneralParticleSource ();

  //default
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();  
  G4ParticleDefinition* particle = particleTable->FindParticle("gamma");
  source->SetParticleDefinition(particle);
  source->SetParticleTime(0.0*ns);
  source->SetParticlePosition(G4ThreeVector(0.0*cm,0.0*cm,0.0*cm));
}

OpticalSimPrimaryGeneratorAction::~OpticalSimPrimaryGeneratorAction()
{
  delete source;
}

void OpticalSimPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    source->GeneratePrimaryVertex(anEvent);
}
