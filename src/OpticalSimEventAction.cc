#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4Timer.hh"

#include "Analysis.hh"
#include "globals.hh"
#include "OpticalSimEventAction.hh"
#include "OpticalSimPMTHit.hh"
#include "OpticalSimScintHit.hh"

#include <vector>

OpticalSimEventAction::OpticalSimEventAction(G4int s_numberOfEvents, G4bool s_verboseEvent, G4double s_shortGatePSD)
	:PMTCollID(-1), scintCollID(-1), numberOfEvents(s_numberOfEvents), verboseEvent(s_verboseEvent), shortGatePSD(s_shortGatePSD),
      isFirstEvent(true)
{    
    eventCount=0;
	timer = new G4Timer;
}

OpticalSimEventAction::~OpticalSimEventAction()
{
	 delete timer;
}

void OpticalSimEventAction::BeginOfEventAction(const G4Event* theEvent)
{
	if (isFirstEvent)
		timer->Start();
    if(PMTCollID<0)
    {
        G4SDManager* SDman = G4SDManager::GetSDMpointer();
        PMTCollID=SDman->GetCollectionID("PMTHitCollection");
    }
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    scintCollID = SDman->GetCollectionID("scintHitCollection");
}

void OpticalSimEventAction::EndOfEventAction(const G4Event* theEvent)
{
    G4HCofThisEvent* hitsCE = theEvent->GetHCofThisEvent();

	//TODO

	int numPDs = 8;
	int numCells = 4;


    if (hitsCE)
    {
		OpticalSimPMTHitsCollection* hitsPMT = (OpticalSimPMTHitsCollection*)(hitsCE->GetHC(PMTCollID));
		OpticalSimScintHitsCollection* hitsScint = (OpticalSimScintHitsCollection*)(hitsCE->GetHC(scintCollID));
		
        if (hitsPMT && hitsScint && hitsScint->entries())
        {
			

			int numPDActivated = hitsPMT->entries();
			int hitCounts[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
			int hitCountsGated[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
			G4double energyDep[] = { 0.0, 0.0, 0.0, 0.0 };
			G4double timeOfHit[] = { -1, -1, -1, -1, -1, -1, -1, -1};
			for (auto i = 0; i < numPDActivated; i++)
			{
				int pdCopyNumber = (*hitsPMT)[i]->GetPDCopyNumber();
				if (pdCopyNumber >= 0)
				{
					int photonCount = (*hitsPMT)[i]->GetPhotonCount();
					int photonCountGated = (*hitsPMT)[i]->GetPhotonCountGated(0, shortGatePSD);
					hitCounts[pdCopyNumber] = photonCount;
					hitCountsGated[pdCopyNumber] = photonCountGated;
					//todo
					//(*hitsPMT)[i]->AddHitTimes(hitTimes, hitSqTimes, numBinsHitTimes, binIntervalHitTimes);
					if (verboseEvent)
						G4cout << "PD [" << pdCopyNumber << "]: " << photonCount << " hits" << G4endl;
				}
			}
			int numScintActivated = hitsScint->entries();
			for (auto i = 0; i < numScintActivated; i++)
			{             
				auto scintHit=(*hitsScint)[i];				
                int scintCopyNumber = scintHit->GetScintCopyNumber();
				if (scintCopyNumber >= 0 && scintCopyNumber<numCells)
				{
                    energyDep[scintCopyNumber] = scintHit->GetEnergy();
					timeOfHit[scintCopyNumber] = scintHit->GetTimeOfFirstHit();
					if (verboseEvent && numScintActivated)
						G4cout << "PD [" << scintCopyNumber << "]: " << energyDep[scintCopyNumber] << " MeV" << G4endl;
				}
			}			
			if (verboseEvent && numScintActivated)
			{
				int totalPhotonCount = 0;
				for (auto photonNum : hitCounts)
					totalPhotonCount += photonNum;

				G4double totalEnergy = 0.0;
				for (auto energy : energyDep)
					totalEnergy += energy;

				G4cout << "Found hits collection: " << totalPhotonCount << " hits" << G4endl << G4endl;
				G4cout << "Found scint hits collection: " << totalEnergy << " MeV" << G4endl << G4endl;
			}

			
			if (numScintActivated)
			{

				//FIX this
				G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
				analysisManager->FillH1(1, eventCount);
				analysisManager->FillH1(2, eventCount);

				//write list mode data (nTuple id=5)
				//analysisManager->FillNtupleIColumn(5, 0, eventCount);
				//Deposited energy [LB, LT, RT, RB] -> [1,2,3,4]
				for (auto i = 0; i < numCells; i++)
					analysisManager->FillNtupleDColumn(5, i, energyDep[i]);
				//Time of first energy deposit [LB, LT, RT, RB] -> [5,6,7,8]
				for (auto i = 0; i < numCells; i++)
					analysisManager->FillNtupleDColumn(5, numCells, timeOfHit[i]);
				//SiPM hit counts
				//Front: [0,1,2,3]->[9,10,11,12]			
				for (auto i = 0; i < numPDs;i++)
					analysisManager->FillNtupleIColumn(5, 2*numCells + i, hitCounts[i]);

				analysisManager->AddNtupleRow(5);
			}
			hitsScint->GetVector()->clear();
        }
        else
        {
            //if (verboseEvent)
                //G4cout<<"Can't find hits collection"<<G4endl<<G4endl;
        }
    }
    if (isFirstEvent)
        eventCount=0;
    isFirstEvent=false;


	G4int eventInterval = 500;
    if (eventCount%eventInterval==0)
	{
		if (eventCount==0)
			G4cout<<"Event "<<eventCount<<" of "<<numberOfEvents<<G4endl;
		else
		{
			timer->Stop();
			G4double rate = eventCount/timer->GetRealElapsed();
			G4double totalSecondsRemaining = (numberOfEvents-eventCount)/rate;
			G4int hoursRemaining = (G4int)(totalSecondsRemaining/3600);
			G4int minsRemaining = (G4int)((totalSecondsRemaining-hoursRemaining*3600)/60);
			G4int secsRemaining = (G4int)(totalSecondsRemaining-hoursRemaining*3600-minsRemaining*60);
			G4cout<<"Event "<<eventCount<<" of "<<numberOfEvents<<": "<<rate<<" events per second, "<<hoursRemaining<<"h"<<minsRemaining<<"m"<<secsRemaining<<"s remaining"<< G4endl;
		}
	}
    eventCount++;
	
}
