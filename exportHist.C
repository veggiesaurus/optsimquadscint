Bool_t exportHist(TString inputFile, TString outputFile, TString cellPrefix = "LT", Int_t numBins=100, Int_t upperBound=6000, Int_t numDataSets=1, Int_t i=0)
{	
	TFile *_file0 = TFile::Open(inputFile);
	TTree *MyTree = (TTree*)_file0->Get("ntuple/EnergyDeps");
      Long64_t numEntries = MyTree->GetEntries();
      TCanvas* c1 = new TCanvas();
      if (numDataSets>1)
      {
            Long64_t entriesPerDataSet=(Long64_t)(numEntries/(float)numDataSets);
            Long64_t firstEntry = i* entriesPerDataSet;
            Long64_t numEntriesInSet = entriesPerDataSet;                  
            //Add all missing entries to last data set
            if (i==numDataSets-1)
                  numEntriesInSet = numEntries;
            MyTree->Draw(Form("sqrt((18980*(1-exp(-.10/.30*%s_Back/18980)))*(18980*(1-exp(-.10/.30*%s_Front/18980))))>>htemp(%d,%d,%d)", cellPrefix.Data(), cellPrefix.Data(), numBins, 0, upperBound), Form("%s_Back>0 && %s_Front >0", cellPrefix.Data(), cellPrefix.Data()), "", numEntriesInSet, firstEntry);
      	SingleExportAscii(htemp, outputFile);
      }
      else
      {
            MyTree->Draw(Form("sqrt((18980*(1-exp(-.10/.30*%s_Back/18980)))*(18980*(1-exp(-.10/.30*%s_Front/18980))))>>htemp(%d,%d,%d)", cellPrefix.Data(), cellPrefix.Data(), numBins, 0, upperBound), Form("%s_Back>0 && %s_Front >0", cellPrefix.Data(), cellPrefix.Data()), "", numEntries, 0);
      	SingleExportAscii(htemp, outputFile);
      }
      
}


Bool_t SingleExportAscii(TH1* hist, TString &filename, TString folder="", TString separator="\t") 
{
   Int_t i,j;
   Double_t xcenter, xwidth;
   Bool_t success=kFALSE;
   //filename = folder + hist->GetName() + ".dat";
   ofstream file_out(filename);
   file_out << "# Output " << hist->ClassName() << ": " << hist->GetName() << " (" << hist->GetTitle() << ")\n";
   if (hist->GetDimension()==1)
   {
      file_out << "# BinCenter" << separator << "Content" << separator << "BinHalfWidth" << separator << "Error\n";
      for (i=1; i<=hist->GetNbinsX(); i++)
         file_out << hist->GetBinCenter(i) << separator << hist->GetBinContent(i) << separator << hist->GetBinWidth(i)/2 << separator << hist->GetBinError(i) << endl;
      if (i>1)
         success=kTRUE;
   }
   else if (hist->GetDimension()==2)
   {
      file_out << "# xBinCenter" << separator << "yBinCenter" << separator << "Content" << separator << "xBinHalfWidth" << separator << "yBinHalfWidth" << separator << "Error" << endl;
      for (i=1; i <= hist->GetNbinsX(); i++)
      {
         xcenter = hist->GetXaxis()->GetBinCenter(i);
         xwidth = hist->GetXaxis()->GetBinWidth(i)/2;
         for (j=1; j <= hist->GetNbinsY(); j++)
            file_out << xcenter << separator << hist->GetYaxis()->GetBinCenter(j) << separator << hist->GetBinContent(i,j) << separator << xwidth << separator << hist->GetYaxis()->GetBinWidth(j)/2 << separator << hist->GetBinError(i,j) << endl;
         if (j>1)
            file_out << endl; // produce a blank line after each set of Ybins for a certain Xbin. Gnuplot likes this.
      }
      if (i>1)
         success=kTRUE;
   }
   file_out.close();
   if (success != kTRUE)
      cout << "Error writing histogram " << hist->GetName() << " written to " << filename << endl;
   return success;
}
