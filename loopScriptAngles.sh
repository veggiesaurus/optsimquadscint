#!/bin/bash
#trap 'kill $(jobs -p)' EXIT
# Batch parameters

N_PROC=4
Z_ROT_DELTA=5
Z_ROT_INIT=0
Z_ROT_FINAL=359
N_EV_MIL=80
JOB_NAME=dummyTest
FILL_WIDTH=44
FILL_THICKNESS=11.5
SEED=1000
HADD_OUTPUT=true
SOURCE_MACRO=source_macros/ambe_neutrons_above.mac
SCINT_RES=4.0
OUTPUT_PREFIX=PST_ambe
ENERGY_OVERRIDE=-1
ENERGY_OVERRIDE_SPREAD=0

while getopts ":d:i:f:n:t:w:r:s:N:J:H:S:O:E:e:" opt; do
  case $opt in    
    d)
      Z_ROT_DELTA=$OPTARG
      ;;
    i)
      Z_ROT_INIT=$OPTARG
      ;;
    f)
      Z_ROT_FINAL=$OPTARG
      ;;    
    n)
      N_EV_MIL=$OPTARG      
      ;;
     t)
      FILL_THICKNESS=$OPTARG
      ;;
    w)
      FILL_WIDTH=$OPTARG
      ;;
    r)
      SCINT_RES=$OPTARG
      ;;
    s)
      SEED=$OPTARG
      ;;
    N)
      N_PROC=$OPTARG      
      ;;
    J)
      JOB_NAME=$OPTARG
      ;;
    H)
      HADD_OUTPUT=$OPTARG
      ;;
     S)
      SOURCE_MACRO=$OPTARG
      ;;    
    O)
      OUTPUT_PREFIX=$OPTARG
      ;;
    E)
      ENERGY_OVERRIDE=$OPTARG
      ;;
    e)
      ENERGY_OVERRIDE_SPREAD=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

for ((Z_ROT=Z_ROT_INIT;Z_ROT<Z_ROT_FINAL;Z_ROT+=Z_ROT_DELTA))
do
    eval ./runScript.sh -N $N_PROC -z $Z_ROT -n $N_EV_MIL -J $JOB_NAME -t $FILL_THICKNESS -w $FILL_WIDTH -s $SEED -H $HADD_OUTPUT -S $SOURCE_MACRO -r $SCINT_RES -O $OUTPUT_PREFIX -E $ENERGY_OVERRIDE -e $ENERGY_OVERRIDE_SPREAD
done
